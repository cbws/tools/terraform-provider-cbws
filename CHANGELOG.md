## [0.3.3](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.3.2...v0.3.3) (2022-08-06)


### Bug Fixes

* **deps:** update module google.golang.org/protobuf to v1.28.1 ([e7306b4](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/e7306b4f872d3e8d2941af78db8bfe6c9d5df95d))

## [0.3.2](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.3.1...v0.3.2) (2022-03-28)


### Bug Fixes

* **deps:** update module google.golang.org/protobuf to v1.28.0 ([6636f50](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/6636f50c0c2f80c0e985e44a5f4ebe25bc830480))

## [0.3.1](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.3.0...v0.3.1) (2021-12-13)


### Bug Fixes

* improve service account key validation ([565af54](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/565af5496c213a55cc460afc934adcccbe091d21))

# [0.3.0](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.2.0...v0.3.0) (2021-12-13)


### Features

* support creating service account keys ([0794163](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/079416331cdeb5f50f88a458518fc9ff6e70ba21))

# [0.2.0](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.1.1...v0.2.0) (2021-12-13)


### Features

* add ability to manage service accounts ([246beb9](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/246beb9d690ff418ab16e48a8c3a2125d624b279))

## [0.1.1](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.1.0...v0.1.1) (2021-12-13)


### Bug Fixes

* improve default credentials error message ([9accb06](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/9accb061b701dde38745ac49130461d5b1d02b03))

# [0.1.0](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.11...v0.1.0) (2021-12-12)


### Features

* support enabling/disabling services on projects ([144329b](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/144329b2912cf8415e1f4fc4ffa696b87c22a2f4))

## [0.0.11](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.10...v0.0.11) (2021-12-12)


### Bug Fixes

* statically compile binary ([2d8ba22](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/2d8ba226a17a7dcf336f788320308de88ab4383e))

## [0.0.10](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.9...v0.0.10) (2021-12-12)


### Bug Fixes

* zip correct arch version ([c975e80](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/c975e80e0e71e65d7481686b8ba6ba9f43e2bf34))

## [0.0.9](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.8...v0.0.9) (2021-12-12)


### Bug Fixes

* upload checksums and signatures of release ([f583fd3](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/f583fd3ef11ee6ef40bc649134ce19120c56240f))

## [0.0.8](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.7...v0.0.8) (2021-12-12)


### Bug Fixes

* use commit tag as version for packages ([44060a7](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/44060a7f318c8d6c01fc11f2bd2eadfba2395170))

## [0.0.7](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.6...v0.0.7) (2021-12-12)


### Bug Fixes

* upload to actual version ([0ebf2e1](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/0ebf2e1801f0726e048e4668d47b66208bc06087))

## [0.0.6](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.5...v0.0.6) (2021-12-12)


### Bug Fixes

* use release-cli to publish packages ([94e1f5b](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/94e1f5b6ed7cdf959b659232ee7596555513a91a))

## [0.0.5](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.4...v0.0.5) (2021-12-12)


### Bug Fixes

* strip v from version in archive ([83c9b5a](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/83c9b5aafe8104c15609fab652a353cc621186de))

## [0.0.4](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.3...v0.0.4) (2021-12-12)


### Bug Fixes

* and now ([cdc1b9c](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/cdc1b9c13f2f7104298f1c177a876dff43a4548d))

## [0.0.3](https://gitlab.com/cbws/tools/terraform-provider-cbws/compare/v0.0.2...v0.0.3) (2021-12-11)


### Bug Fixes

* try publishing assets ([4252678](https://gitlab.com/cbws/tools/terraform-provider-cbws/commit/425267831e0dedd1df52d333d4d0a165bf65042f))
