resource "cbws_service_management_permission" "acceptance" {
  service      = "test.ci.cbws.dev"
  type         = "TestEntity"
  action       = "action"
  description  = "This permission tests the Terraform provider"
}
resource "cbws_service_management_role" "acceptance" {
  service      = "test.ci.cbws.dev"
  role         = "test"
  display_name = "A role to test Terraform"
  description  = "This role tests the Terraform provider"
  permissions  = [
    "services/iam.cbws.xyz/permissions/ServiceAccount.create",
    cbws_service_management_permission.acceptance.name,
  ]
}
