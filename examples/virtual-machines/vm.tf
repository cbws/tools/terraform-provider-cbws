provider "cbws" {
  //  organization = "908e6132-1eb9-11ea-939b-9c81b2f6bed2"
  project = "playground"
}

//resource "cbws_virtual_machines_instance" "web" {
//  name = format("web%01s", count.index+1)
//  hostname = "web${count.index+1}.marlinc.nl"
//  cpu = 1
//  memory = 1024
//  root_disk = 20
//  ssh_keys = [
//    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDS0v3SXLigOwAz9T/rUpzmq+qTScaPLLLYjZv9bGgjf9Ay86+ZCngz0bA5RItbrsEEvIO1GXyJhKH8bInwmxjk/gKPXHh3QTeEjl0y38bmyaTSJtlum0myohZAkY9vDcOASbckMr8xcxP8d87/qDswdA3Ni0cerBi9XWw0Ru8qRtsPR4kNd/yGKrwPvjRkKLrIPazNd8d3JusfpgyayYFOqlDkVEcAU6Ddr+JHEhOTWoLH6VDCr8b5yl8bma2jv/gMOkjBPIQqUqqyj22sKmKdrnqnek2kCvSCfr3yn5GOloGg8COfKwjcRNmfI1orDd7AUhbkNsf1aoXR3yAXRrFoNBAjti3NHr65qfpETuE++KDHS0sd3bF4dI5+juFYe5HezkK9+2DmZ3vp2H7LDOhwMV2JI18HIYuUdmO4/8ziKDzCi6TEJ3FGrSbXVTXMVCXgP2xQpKaW1ZwOyT46Mb2KMmszgpLQKOtRg02/HsFM5nKx9L3t1b0IAhcxuh7W5sdTLtY6Hx+W124NeB2PDosdr0/iqie8qnUrCNzPNsUNNpHLAqTNUUfpKzZs0E2A9E7S/6bbRra/5ceiI4+5yR31nvEMawMcUbvI2c+QMjeNzZ7BzyK3q7lVbzfGeC8KA2UZEFrNEw2DenMOE9e7HOCea7YO1Zp6/E0dx39ygC+CTQ=="
//    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC079ZWo9u+GKGNx6KCduTFemQADX3snuY3ehg9bPR99X6YT+Y4ZMrUDc2UZa0azCOq8iERey9JxVS5WC4KlbLlPzUIY75G3lSSwNoFOLp4KV5FK2i0+L6MqgjwtttX7/M+qeEW8RWXUm/VeW+RSJM41TxjkBzJACAdeEccVLL5y3ohcw/Fd99RyCSw66VYU6Klr9307SblPxDhxHySBNOJEYFtPSHdXnj/kdr1EkMpmL7nwL+JyZ7vx134+w7xhCVgcmF6v9jJN/MSmEmt03fs5+j+FdfW6e2wMMBFHCyTP9S3TDAOA9be9SSH6l0f9zy9F+vHiUY4CosvoD2vuE7N customer@vpsbenchmarks.com"
//  ]
//  count = 1
//  labels = {
//    app = "web"
//  }
////  provisioner "remote-exec" {
////    inline = [
////      "echo Test!",
////    ]
////  }
//}

resource "cbws_virtual_machines_instance" "vpsbenchmarks" {
  name = "vpsbenchmarks"
  hostname = "vpsbenchmarks.marlinc.nl"
  cpu = 1
  memory = 2048
  root_disk = 30
  ssh_keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDS0v3SXLigOwAz9T/rUpzmq+qTScaPLLLYjZv9bGgjf9Ay86+ZCngz0bA5RItbrsEEvIO1GXyJhKH8bInwmxjk/gKPXHh3QTeEjl0y38bmyaTSJtlum0myohZAkY9vDcOASbckMr8xcxP8d87/qDswdA3Ni0cerBi9XWw0Ru8qRtsPR4kNd/yGKrwPvjRkKLrIPazNd8d3JusfpgyayYFOqlDkVEcAU6Ddr+JHEhOTWoLH6VDCr8b5yl8bma2jv/gMOkjBPIQqUqqyj22sKmKdrnqnek2kCvSCfr3yn5GOloGg8COfKwjcRNmfI1orDd7AUhbkNsf1aoXR3yAXRrFoNBAjti3NHr65qfpETuE++KDHS0sd3bF4dI5+juFYe5HezkK9+2DmZ3vp2H7LDOhwMV2JI18HIYuUdmO4/8ziKDzCi6TEJ3FGrSbXVTXMVCXgP2xQpKaW1ZwOyT46Mb2KMmszgpLQKOtRg02/HsFM5nKx9L3t1b0IAhcxuh7W5sdTLtY6Hx+W124NeB2PDosdr0/iqie8qnUrCNzPNsUNNpHLAqTNUUfpKzZs0E2A9E7S/6bbRra/5ceiI4+5yR31nvEMawMcUbvI2c+QMjeNzZ7BzyK3q7lVbzfGeC8KA2UZEFrNEw2DenMOE9e7HOCea7YO1Zp6/E0dx39ygC+CTQ=="
  ]
  count = 1
  labels = {
    app = "web"
  }
  //  provisioner "remote-exec" {
  //    inline = [
  //      "echo Test!",
  //    ]
  //  }
}

resource "cbws_virtual_machines_instance" "bench" {
  name = "bench"
  hostname = "bench.marlinc.nl"
  cpu = 1
  memory = 2048
  root_disk = 30
  ssh_keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDS0v3SXLigOwAz9T/rUpzmq+qTScaPLLLYjZv9bGgjf9Ay86+ZCngz0bA5RItbrsEEvIO1GXyJhKH8bInwmxjk/gKPXHh3QTeEjl0y38bmyaTSJtlum0myohZAkY9vDcOASbckMr8xcxP8d87/qDswdA3Ni0cerBi9XWw0Ru8qRtsPR4kNd/yGKrwPvjRkKLrIPazNd8d3JusfpgyayYFOqlDkVEcAU6Ddr+JHEhOTWoLH6VDCr8b5yl8bma2jv/gMOkjBPIQqUqqyj22sKmKdrnqnek2kCvSCfr3yn5GOloGg8COfKwjcRNmfI1orDd7AUhbkNsf1aoXR3yAXRrFoNBAjti3NHr65qfpETuE++KDHS0sd3bF4dI5+juFYe5HezkK9+2DmZ3vp2H7LDOhwMV2JI18HIYuUdmO4/8ziKDzCi6TEJ3FGrSbXVTXMVCXgP2xQpKaW1ZwOyT46Mb2KMmszgpLQKOtRg02/HsFM5nKx9L3t1b0IAhcxuh7W5sdTLtY6Hx+W124NeB2PDosdr0/iqie8qnUrCNzPNsUNNpHLAqTNUUfpKzZs0E2A9E7S/6bbRra/5ceiI4+5yR31nvEMawMcUbvI2c+QMjeNzZ7BzyK3q7lVbzfGeC8KA2UZEFrNEw2DenMOE9e7HOCea7YO1Zp6/E0dx39ygC+CTQ=="
  ]
  count = 1
  labels = {
    app = "web"
  }
  //  provisioner "remote-exec" {
  //    inline = [
  //      "echo Test!",
  //    ]
  //  }
}

resource "cbws_virtual_machines_instance" "teststuff" {
  name = "teststuff"
  hostname = "teststuff.marlinc.nl"
  cpu = 1
  memory = 2048
  root_disk = 30
  ssh_keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDS0v3SXLigOwAz9T/rUpzmq+qTScaPLLLYjZv9bGgjf9Ay86+ZCngz0bA5RItbrsEEvIO1GXyJhKH8bInwmxjk/gKPXHh3QTeEjl0y38bmyaTSJtlum0myohZAkY9vDcOASbckMr8xcxP8d87/qDswdA3Ni0cerBi9XWw0Ru8qRtsPR4kNd/yGKrwPvjRkKLrIPazNd8d3JusfpgyayYFOqlDkVEcAU6Ddr+JHEhOTWoLH6VDCr8b5yl8bma2jv/gMOkjBPIQqUqqyj22sKmKdrnqnek2kCvSCfr3yn5GOloGg8COfKwjcRNmfI1orDd7AUhbkNsf1aoXR3yAXRrFoNBAjti3NHr65qfpETuE++KDHS0sd3bF4dI5+juFYe5HezkK9+2DmZ3vp2H7LDOhwMV2JI18HIYuUdmO4/8ziKDzCi6TEJ3FGrSbXVTXMVCXgP2xQpKaW1ZwOyT46Mb2KMmszgpLQKOtRg02/HsFM5nKx9L3t1b0IAhcxuh7W5sdTLtY6Hx+W124NeB2PDosdr0/iqie8qnUrCNzPNsUNNpHLAqTNUUfpKzZs0E2A9E7S/6bbRra/5ceiI4+5yR31nvEMawMcUbvI2c+QMjeNzZ7BzyK3q7lVbzfGeC8KA2UZEFrNEw2DenMOE9e7HOCea7YO1Zp6/E0dx39ygC+CTQ=="
  ]
  count = 1
  labels = {
    app = "web"
  }
  //  provisioner "remote-exec" {
  //    inline = [
  //      "echo Test!",
  //    ]
  //  }
}

resource "cbws_virtual_machines_instance" "teststuff2" {
  name = "teststuff2"
  hostname = "teststuff2.marlinc.nl"
  cpu = 1
  memory = 2048
  root_disk = 30
  ssh_keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDS0v3SXLigOwAz9T/rUpzmq+qTScaPLLLYjZv9bGgjf9Ay86+ZCngz0bA5RItbrsEEvIO1GXyJhKH8bInwmxjk/gKPXHh3QTeEjl0y38bmyaTSJtlum0myohZAkY9vDcOASbckMr8xcxP8d87/qDswdA3Ni0cerBi9XWw0Ru8qRtsPR4kNd/yGKrwPvjRkKLrIPazNd8d3JusfpgyayYFOqlDkVEcAU6Ddr+JHEhOTWoLH6VDCr8b5yl8bma2jv/gMOkjBPIQqUqqyj22sKmKdrnqnek2kCvSCfr3yn5GOloGg8COfKwjcRNmfI1orDd7AUhbkNsf1aoXR3yAXRrFoNBAjti3NHr65qfpETuE++KDHS0sd3bF4dI5+juFYe5HezkK9+2DmZ3vp2H7LDOhwMV2JI18HIYuUdmO4/8ziKDzCi6TEJ3FGrSbXVTXMVCXgP2xQpKaW1ZwOyT46Mb2KMmszgpLQKOtRg02/HsFM5nKx9L3t1b0IAhcxuh7W5sdTLtY6Hx+W124NeB2PDosdr0/iqie8qnUrCNzPNsUNNpHLAqTNUUfpKzZs0E2A9E7S/6bbRra/5ceiI4+5yR31nvEMawMcUbvI2c+QMjeNzZ7BzyK3q7lVbzfGeC8KA2UZEFrNEw2DenMOE9e7HOCea7YO1Zp6/E0dx39ygC+CTQ=="
  ]
  count = 1
  labels = {
    app = "web"
  }
  affinity {
    node_affinity {
      preferred_during_scheduling_ignored_during_execution {
        preferred_scheduling_term {
          preference {

          }
          weight = 10
        }
      }
    }
  }
  //  provisioner "remote-exec" {
  //    inline = [
  //      "echo Test!",
  //    ]
  //  }
}
