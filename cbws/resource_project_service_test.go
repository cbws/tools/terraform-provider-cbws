package cbws

import (
	"context"
	"fmt"
	"log"
	"strings"
	"testing"

	"github.com/cbws/go-cbws-grpc/cbws/projects/v1alpha1"
	"github.com/cbws/go-pagination"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"

	"github.com/cbws/terraform-provider-cbws/cbws/service_usage"
)

func init() {
	resource.AddTestSweepers("ProjectService", &resource.Sweeper{
		Name: "ProjectService",
		F:    testSweepProject,
	})
}

func testSweepProjectService(region string) error {
	config, err := sharedConfigForRegion(region)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error getting shared config for region: %s", err)
		return err
	}

	err = config.LoadAndValidate(context.Background())
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error loading: %s", err)
		return err
	}

	paginator := config.clientProjects.PaginateProjects()
	projects, err := pagination.Iterate(context.Background(), paginator)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] Error listing projects: %s", err)
		return nil
	}
	for _, edge := range projects {
		project := edge.Node.(*v1alpha1.Project)
		projectName := strings.Split(project.Name, "/")[1]
		if !strings.HasPrefix(projectName, testPrefix) {
			log.Printf("[INFO][SWEEPER_LOG] Not sweeping non test project: %s", project.Name)

			continue
		}

		log.Printf("[INFO][SWEEPER_LOG] Sweeping project service: %s", project.Name)

		servicePaginator := config.clientServiceUsage.PaginateServices(projectName)
		services, err := pagination.Iterate(context.Background(), servicePaginator)
		if err != nil {
			log.Printf("[INFO][SWEEPER_LOG] Error listing project services: %s", err)
			return nil
		}
		for _, serviceEdge := range services {
			service := serviceEdge.Node.(service_usage.Service)
			if service_usage.ServiceState(service.State).Disabled() {
				continue
			}

			log.Printf("[INFO][SWEEPER_LOG] Sweeping service: %s", service.Name)

			err = config.clientServiceUsage.DisableService(context.Background(), projectName, service.Config.Name)
			if err != nil {
				log.Printf("[INFO][SWEEPER_LOG] Error, failed to disable service %s: %s", service.Name, err)
				continue
			}
		}
	}

	return nil
}

// Test that a Project service resource can be created
func TestAccProjectService_create(t *testing.T) {
	t.Parallel()

	org := getTestOrgFromEnv(t)
	pid := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	service := "iam.cbws.xyz"
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccProjectService_create(pid, name, org, service),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckProjectExists("cbws_project.acceptance", pid),
					testAccCheckProjectServiceExists("cbws_project_service.acceptance", pid, service),
				),
			},
		},
	})
}

func testAccCheckProjectServiceExists(r, pid, service string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[r]
		if !ok {
			return fmt.Errorf("Not found: %s", r)
		}

		if rs.Primary.ID == "" {
			return fmt.Errorf("No ID is set")
		}

		projectId := fmt.Sprintf("projects/%s/services/%s", pid, service)
		if rs.Primary.ID != projectId {
			return fmt.Errorf("Expected project %q to match ID %q in state", projectId, rs.Primary.ID)
		}

		return nil
	}
}

func testAccProjectService_create(pid, name, org, service string) string {
	return fmt.Sprintf(`
resource "cbws_project" "acceptance" {
  project      = "%s"
  display_name = "%s"
  organization = "%s"
}

resource "cbws_project_service" "acceptance" {
  project      = cbws_project.acceptance.project
  service      = "%s"
}
`, pid, name, org, service)
}
