package cbws

import (
	"context"
	"log"
	"strings"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/cbws/terraform-provider-cbws/cbws/iam_backchannel"
)

// resourceServiceManagementRole returns a *schema.Resource that allows a customer
// to declare a service management IAM role resource.
func resourceServiceManagementRole() *schema.Resource {
	return &schema.Resource{
		SchemaVersion: 1,

		Create: resourceServiceManagementRoleCreate,
		Read:   resourceServiceManagementRoleRead,
		Update: resourceServiceManagementRoleUpdate,
		Delete: resourceServiceManagementRoleDelete,
		Exists: resourceServiceManagementRoleExists,

		Importer: &schema.ResourceImporter{
			State: resourceServiceManagementRoleImportState,
		},

		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(5 * time.Minute),
			Read:   schema.DefaultTimeout(15 * time.Second),
			Delete: schema.DefaultTimeout(1 * time.Minute),
		},

		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The resource name of the role.",
			},
			"service": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateHostname(),
				Description:  "The service to which this role will belong.",
			},
			"role": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateName(),
				Description:  "The name of the role. Changing this forces a new role to be created.",
			},
			"display_name": {
				Type:        schema.TypeString,
				Optional:    true,
				Description: "The name to show to users.",
			},
			"description": {
				Type:        schema.TypeString,
				Optional:    true,
				Description: "The description of what this role allows principals to do.",
			},
			"permissions": {
				Type:     schema.TypeList,
				Required: true,
				Elem: &schema.Schema{
					Type:        schema.TypeString,
					Description: "A permission in the format services/iam.cbws.cloud/permissions/ServiceAccount.create",
				},
				Description: "The permissions this role should have.",
			},
		},
	}
}

func resourceServiceManagementRoleCreate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	service := d.Get("service").(string)
	roleName := d.Get("role").(string)

	log.Printf("[DEBUG]: Creating new role %s", roleName)

	var permissions []string
	for _, sshKey := range d.Get("permissions").([]interface{}) {
		permissions = append(permissions, sshKey.(string))
	}

	ctx := context.Background()

	log.Printf("D: %+v", d)

	var opts []iam_backchannel.CreateRoleOption
	if value := d.Get("display_name").(string); value != "" {
		opts = append(opts, iam_backchannel.WithCreateRoleDisplayName(value))
	}
	if value := d.Get("description").(string); value != "" {
		opts = append(opts, iam_backchannel.WithCreateRoleDescription(value))
	}

	role, err := config.clientIAMBackchannel.CreateRole(ctx, service, roleName, permissions, opts...)
	if err != nil {
		return err
	}

	resourceServiceManagementRoleData(role, d)

	return nil
}

func resourceServiceManagementRoleRead(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	service := d.Get("service").(string)
	roleName := d.Get("role").(string)

	log.Printf("[DEBUG]: Getting role %s", roleName)

	ctx := context.Background()
	role, err := config.clientIAMBackchannel.GetRole(ctx, service, roleName)
	if err != nil {
		return err
	}

	resourceServiceManagementRoleData(role, d)

	return nil
}

func resourceServiceManagementRoleUpdate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	service := d.Get("service").(string)
	roleName := d.Get("name").(string)

	log.Printf("[DEBUG]: Updating role %s", roleName)

	var opts []iam_backchannel.UpdateRoleOption
	if d.HasChange("display_name") {
		opts = append(opts, iam_backchannel.WithUpdateRoleDisplayName(d.Get("display_name").(string)))
	}
	if d.HasChange("description") {
		opts = append(opts, iam_backchannel.WithUpdateRoleDescription(d.Get("description").(string)))
	}

	ctx := context.Background()
	role, err := config.clientIAMBackchannel.UpdateRole(ctx, service, roleName, opts...)
	if err != nil {
		return err
	}

	resourceServiceManagementRoleData(role, d)

	return nil
}

func resourceServiceManagementRoleDelete(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	service := d.Get("service").(string)
	roleName := d.Get("role").(string)

	log.Printf("[DEBUG]: Deleting role %s", roleName)

	ctx := context.Background()
	err := config.clientIAMBackchannel.DeleteRole(ctx, service, roleName)
	if err != nil {
		return err
	}

	d.SetId("")

	return nil
}

func resourceServiceManagementRoleExists(d *schema.ResourceData, meta interface{}) (bool, error) {
	config := meta.(*Config)
	service := d.Get("service").(string)
	roleName := d.Get("role").(string)

	log.Printf("[DEBUG]: Checking role existance %s", roleName)

	ctx := context.Background()
	role, err := config.clientIAMBackchannel.GetRole(ctx, service, roleName)
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.NotFound:
				return false, nil
			}
		}
		return false, err
	}

	resourceServiceManagementRoleData(role, d)

	return true, nil
}

func resourceServiceManagementRoleImportState(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	config := meta.(*Config)
	split := strings.Split(d.Id(), "/")

	if len(split) != 4 || split[0] != "services" || split[2] != "roles" {
		return nil, status.Error(codes.InvalidArgument, "id should be in format of services/iam.cbws.cloud/roles/role")
	}

	service := split[1]
	roleName := split[3]

	ctx := context.Background()
	role, err := config.clientIAMBackchannel.GetRole(ctx, service, roleName)
	if err != nil {
		return nil, err
	}

	resourceServiceManagementRoleData(role, d)

	return []*schema.ResourceData{d}, nil
}

func resourceServiceManagementRoleData(r iam_backchannel.Role, d *schema.ResourceData) {
	d.SetId(r.Name)
	d.Set("name", r.Name)
	d.Set("role", r.Role)
	d.Set("permissions", r.Permissions)
	if r.DisplayName != "" {
		d.Set("display_name", r.DisplayName)
	} else {
		d.Set("display_name", nil)
	}
	if r.Description != "" {
		d.Set("description", r.Description)
	} else {
		d.Set("description", nil)
	}
}
