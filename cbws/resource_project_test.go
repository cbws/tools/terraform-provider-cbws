package cbws

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/cbws/go-cbws-grpc/cbws/projects/v1alpha1"

	"github.com/cbws/go-pagination"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
)

var (
	name       = "Terraform Acceptance Tests"
	testPrefix = "tf-test"
)

func init() {
	resource.AddTestSweepers("Project", &resource.Sweeper{
		Name:         "Project",
		F:            testSweepProject,
		Dependencies: []string{"ProjectService"},
	})
}

func testSweepProject(region string) error {
	config, err := sharedConfigForRegion(region)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error getting shared config for region: %s", err)
		return err
	}

	err = config.LoadAndValidate(context.Background())
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error loading: %s", err)
		return err
	}

	paginator := config.clientProjects.PaginateProjects()
	projects, err := pagination.Iterate(context.Background(), paginator)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] Error listing projects: %s", err)
		return nil
	}
	for _, edge := range projects {
		project := edge.Node.(*v1alpha1.Project)
		projectName := strings.Split(project.Name, "/")[1]
		if !strings.HasPrefix(projectName, testPrefix) {
			log.Printf("[INFO][SWEEPER_LOG] Not sweeping non test project: %s", project.Name)

			continue
		}

		log.Printf("[INFO][SWEEPER_LOG] Sweeping project: %s", project.Name)

		_, err := config.clientProjects.DeleteProject(context.Background(), projectName)
		if err != nil {
			log.Printf("[INFO][SWEEPER_LOG] Error, failed to delete project %s: %s", projectName, err)
			continue
		}
	}

	return nil
}

// Test that a Project resource can be created and an IAM policy
// associated
func TestAccProject_create(t *testing.T) {
	t.Parallel()

	org := getTestOrgFromEnv(t)
	pid := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new project
			{
				Config: testAccProject_create(pid, name, org),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckProjectExists("cbws_project.acceptance", pid),
				),
			},
		},
	})
}

func testAccCheckProjectExists(r, pid string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[r]
		if !ok {
			return fmt.Errorf("Not found: %s", r)
		}

		if rs.Primary.ID == "" {
			return fmt.Errorf("No ID is set")
		}

		projectId := fmt.Sprintf("projects/%s", pid)
		if rs.Primary.ID != projectId {
			return fmt.Errorf("Expected project %q to match ID %q in state", projectId, rs.Primary.ID)
		}

		return nil
	}
}

func testAccProject_create(pid, name, org string) string {
	return fmt.Sprintf(`
resource "cbws_project" "acceptance" {
  project      = "%s"
  display_name = "%s"
  organization = "%s"
}
`, pid, name, org)
}

func skipIfEnvNotSet(t *testing.T, envs ...string) {
	if t == nil {
		log.Printf("[DEBUG] Not running inside of test - skip skipping")
		return
	}

	for _, k := range envs {
		if os.Getenv(k) == "" {
			t.Skipf("Environment variable %s is not set", k)
		}
	}
}
