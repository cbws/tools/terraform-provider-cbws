package cbws

import (
	"context"
	"fmt"
	"log"
	"strings"
	"testing"

	pbvirtualmachines "github.com/cbws/go-cbws/cbws/virtual-machines/v1alpha1"
	"github.com/cbws/go-pagination"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
)

//func init() {
//	resource.AddTestSweepers("VirtualMachineInstance", &resource.Sweeper{
//		Name: "VirtualMachineInstance",
//		F:    testSweepVirtualMachine,
//	})
//}

func testSweepVirtualMachine(region string) error {
	config, err := sharedConfigForRegion(region)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error getting shared config for region: %s", err)
		return err
	}

	err = config.LoadAndValidate(context.Background())
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error loading: %s", err)
		return err
	}

	paginator := config.clientVirtualMachines.Paginate()
	instances, err := pagination.Iterate(context.Background(), paginator)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] Error listing virtual machine instances: %s", err)
		return err
	}
	for _, edge := range instances {
		instance := edge.Node.(pbvirtualmachines.Instance)

		resourceName := strings.Split(instance.Name, "/")[3]
		if !isSweepableTestResource(resourceName) {
			log.Printf("[INFO][SWEEPER_LOG] Not sweeping non test virtual machine instance: %s", resourceName)

			continue
		}

		operationHandler, err := config.clientVirtualMachines.Delete(context.Background(), resourceName)
		if err != nil {
			return err
		}
		st, err := operationHandler.Wait(context.Background())
		if err != nil {
			return err
		}
		if st != nil {
			return fmt.Errorf("delete failed: %+v", st)
		}
	}

	return nil
}

// Test that a Project resource can be created and an IAM policy
// associated
func TestAccVMInstance_create(t *testing.T) {
	t.SkipNow()
	t.Parallel()

	pid := getTestProjectFromEnv()
	name := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new instance
			{
				Config: testAccVMInstance_create(name),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckVMInstanceExists("cbws_virtual_machines_instance.acceptance", pid, name),
				),
			},
		},
	})
}

func testAccCheckVMInstanceExists(r, pid, name string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[r]
		if !ok {
			return fmt.Errorf("Not found: %s", r)
		}

		if rs.Primary.ID == "" {
			return fmt.Errorf("No ID is set")
		}

		projectId := fmt.Sprintf("projects/%s/instances/%s", pid, name)
		if rs.Primary.ID != projectId {
			return fmt.Errorf("Expected project %q to match ID %q in state", projectId, rs.Primary.ID)
		}

		return nil
	}
}

func testAccVMInstance_create(name string) string {
	return fmt.Sprintf(`
resource "cbws_virtual_machines_instance" "acceptance" {
  name      = "%s"
  hostname  = "%s.example.com"
  cpu       = 1
  memory    = 256
  root_disk = 2
}
`, name, name)
}
