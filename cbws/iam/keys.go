package iam

import (
	"context"
	"fmt"
	"strings"

	"github.com/cbws/go-cbws-grpc/cbws/iam/v1alpha1"
	"github.com/cbws/go-pagination"
)

type ServiceAccountKey *v1alpha1.ServiceAccountKey

type ListServiceAccountKeyOption interface {
	set(r *v1alpha1.ListServiceAccountKeysRequest)
}

func (c Client) PaginateServiceAccountKeys(email string, opts ...ListServiceAccountKeyOption) pagination.Paginator {
	r := &v1alpha1.ListServiceAccountKeysRequest{
		Parent: fmt.Sprintf("projects/%s/serviceAccounts/%s", c.project, email),
	}
	for _, opt := range opts {
		opt.set(r)
	}

	return func(ctx context.Context, first, last *int, before, after pagination.Cursor) (*pagination.Connection, error) {
		connection := &pagination.Connection{
			PageInfo: pagination.PageInfo{},
		}

		response, err := c.client.ListServiceAccountKeys(ctx, r)
		if err != nil {
			return nil, err
		}
		for _, serviceAccountKey := range response.Keys {
			connection.Edges = append(connection.Edges, pagination.Edge{
				Node: ServiceAccountKey(serviceAccountKey),
			})
		}

		return connection, nil
	}
}

type createServiceAccountKeyProject string

func (c createServiceAccountKeyProject) set(request *v1alpha1.CreateServiceAccountKeyRequest) {
	request.Parent = fmt.Sprintf("projects/%s/serviceAccounts/%s", c, strings.Split(request.Parent, "/")[3])
}

func WithCreateServiceAccountKeyProject(displayName string) CreateServiceAccountKeyOption {
	return createServiceAccountKeyProject(displayName)
}

type CreateServiceAccountKeyOption interface {
	set(*v1alpha1.CreateServiceAccountKeyRequest)
}

func (c *Client) CreateServiceAccountKey(ctx context.Context, email string, opts ...CreateServiceAccountKeyOption) (ServiceAccountKey, error) {
	request := &v1alpha1.CreateServiceAccountKeyRequest{
		Parent: fmt.Sprintf("projects/%s/serviceAccounts/%s", c.project, email),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.CreateServiceAccountKey(ctx, request)
}

type getServiceAccountKeyProject string

func (c getServiceAccountKeyProject) set(request *v1alpha1.GetServiceAccountKeyRequest) {
	request.Name = fmt.Sprintf("projects/%s/serviceAccounts/%s/keys/%s", c, strings.Split(request.Name, "/")[3], strings.Split(request.Name, "/")[5])
}

func WithGetServiceAccountKeyProject(displayName string) GetServiceAccountKeyOption {
	return getServiceAccountKeyProject(displayName)
}

type GetServiceAccountKeyOption interface {
	set(request *v1alpha1.GetServiceAccountKeyRequest)
}

func (c *Client) GetServiceAccountKey(ctx context.Context, email, uniqueId string, opts ...GetServiceAccountKeyOption) (ServiceAccountKey, error) {
	request := &v1alpha1.GetServiceAccountKeyRequest{
		Name: fmt.Sprintf("projects/%s/serviceAccounts/%s/keys/%s", c.project, email, uniqueId),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.GetServiceAccountKey(ctx, request)
}

type deleteServiceAccountKeyProject string

func (c deleteServiceAccountKeyProject) set(request *v1alpha1.DeleteServiceAccountKeyRequest) {
	request.Name = fmt.Sprintf("projects/%s/serviceAccounts/%s/keys/%s", c, strings.Split(request.Name, "/")[3], strings.Split(request.Name, "/")[5])
}

func WithDeleteServiceAccountKeyProject(displayName string) DeleteServiceAccountKeyOption {
	return deleteServiceAccountKeyProject(displayName)
}

type DeleteServiceAccountKeyOption interface {
	set(request *v1alpha1.DeleteServiceAccountKeyRequest)
}

func (c *Client) DeleteServiceAccountKey(ctx context.Context, email, uniqueId string, opts ...DeleteServiceAccountKeyOption) error {
	request := &v1alpha1.DeleteServiceAccountKeyRequest{
		Name: fmt.Sprintf("projects/%s/serviceAccounts/%s/keys/%s", c.project, email, uniqueId),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	_, err := c.client.DeleteServiceAccountKey(ctx, request)
	return err
}
