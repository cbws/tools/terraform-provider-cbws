package iam

import (
	"context"
	"fmt"
	"io"
	"strings"

	"github.com/cbws/go-cbws-grpc/cbws/iam/v1alpha1"
	"github.com/cbws/go-cbws/client/grpc"
	"github.com/cbws/go-cbws/option"
	"github.com/cbws/go-pagination"
)

const endpoint = "iam.cbws.cloud:443"

func NewClient(ctx context.Context, project string, opts ...option.ClientOption) (*Client, error) {
	o := []option.ClientOption{
		option.WithEndpoint(endpoint),
		option.WithUserAgent("terraform-provider-cbws"),
	}
	o = append(o, opts...)

	conn, err := grpc.New(ctx, o...)
	if err != nil {
		return nil, err
	}

	return &Client{
		clientConn: conn,
		client:     v1alpha1.NewIAMServiceClient(conn),
		project:    project,
	}, nil
}

type Client struct {
	client     v1alpha1.IAMServiceClient
	clientConn io.Closer
	project    string
}

func (c *Client) Close() error {
	return c.clientConn.Close()
}

type ServiceAccount *v1alpha1.ServiceAccount

type ListServiceAccountOption interface {
	set(r *v1alpha1.ListServiceAccountsRequest)
}

func (c Client) PaginateServiceAccounts(opts ...ListServiceAccountOption) pagination.Paginator {
	r := &v1alpha1.ListServiceAccountsRequest{
		Parent: fmt.Sprintf("projects/%s", c.project),
	}
	for _, opt := range opts {
		opt.set(r)
	}

	return func(ctx context.Context, first, last *int, before, after pagination.Cursor) (*pagination.Connection, error) {
		connection := &pagination.Connection{
			PageInfo: pagination.PageInfo{},
		}
		if first != nil {
			r.PageSize = int32(*first)
		}
		if after != nil {
			r.PageToken = after.GetCursor()
		}

		response, err := c.client.ListServiceAccounts(ctx, r)
		if err != nil {
			return nil, err
		}
		for _, serviceAccount := range response.Accounts {
			connection.Edges = append(connection.Edges, pagination.Edge{
				Node: ServiceAccount(serviceAccount),
			})
		}
		connection.PageInfo.EndCursor = pagination.StringCursor(response.NextPageToken)
		connection.PageInfo.HasNextPage = response.NextPageToken != ""

		return connection, nil
	}
}

type createServiceAccountDisplayName string

func (c createServiceAccountDisplayName) set(request *v1alpha1.CreateServiceAccountRequest) {
	request.ServiceAccount.DisplayName = string(c)
}

func WithCreateServiceAccountDisplayName(displayName string) CreateServiceAccountOption {
	return createServiceAccountDisplayName(displayName)
}

type createServiceAccountProject string

func (c createServiceAccountProject) set(request *v1alpha1.CreateServiceAccountRequest) {
	request.Parent = fmt.Sprintf("projects/%s", c)
}

func WithCreateServiceAccountProject(displayName string) CreateServiceAccountOption {
	return createServiceAccountProject(displayName)
}

type CreateServiceAccountOption interface {
	set(*v1alpha1.CreateServiceAccountRequest)
}

func (c *Client) CreateServiceAccount(ctx context.Context, accountID string, opts ...CreateServiceAccountOption) (ServiceAccount, error) {
	request := &v1alpha1.CreateServiceAccountRequest{
		Parent:         "projects/" + c.project,
		AccountId:      accountID,
		ServiceAccount: &v1alpha1.ServiceAccount{},
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.CreateServiceAccount(ctx, request)
}

type getServiceAccountProject string

func (c getServiceAccountProject) set(request *v1alpha1.GetServiceAccountRequest) {
	request.Name = fmt.Sprintf("projects/%s/serviceAccounts/%s", c, strings.Split(request.Name, "/")[3])
}

func WithGetServiceAccountProject(displayName string) GetServiceAccountOption {
	return getServiceAccountProject(displayName)
}

type GetServiceAccountOption interface {
	set(request *v1alpha1.GetServiceAccountRequest)
}

func (c *Client) GetServiceAccount(ctx context.Context, email string, opts ...GetServiceAccountOption) (ServiceAccount, error) {
	request := &v1alpha1.GetServiceAccountRequest{
		Name: fmt.Sprintf("projects/%s/serviceAccounts/%s", c.project, email),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.GetServiceAccount(ctx, request)
}

type updateServiceAccountDisplayName string

func (c updateServiceAccountDisplayName) set(request *v1alpha1.UpdateServiceAccountRequest) {
	request.ServiceAccount.DisplayName = string(c)
}

func WithUpdateServiceAccountDisplayName(displayName string) UpdateServiceAccountOption {
	return updateServiceAccountDisplayName(displayName)
}

type updateServiceAccountProject string

func (c updateServiceAccountProject) set(request *v1alpha1.UpdateServiceAccountRequest) {
	request.ServiceAccount.Name = fmt.Sprintf("projects/%s/serviceAccounts/%s", c, strings.Split(request.ServiceAccount.Name, "/")[3])
}

func WithUpdateServiceAccountProject(displayName string) UpdateServiceAccountOption {
	return updateServiceAccountProject(displayName)
}

type UpdateServiceAccountOption interface {
	set(request *v1alpha1.UpdateServiceAccountRequest)
}

func (c *Client) UpdateServiceAccount(ctx context.Context, email string, opts ...UpdateServiceAccountOption) (ServiceAccount, error) {
	request := &v1alpha1.UpdateServiceAccountRequest{
		ServiceAccount: &v1alpha1.ServiceAccount{
			Name: fmt.Sprintf("projects/%s/serviceAccounts/%s", c.project, email),
		},
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.UpdateServiceAccount(ctx, request)
}

type deleteServiceAccountProject string

func (c deleteServiceAccountProject) set(request *v1alpha1.DeleteServiceAccountRequest) {
	request.Name = fmt.Sprintf("projects/%s/serviceAccounts/%s", c, strings.Split(request.Name, "/")[3])
}

func WithDeleteServiceAccountProject(displayName string) DeleteServiceAccountOption {
	return deleteServiceAccountProject(displayName)
}

type DeleteServiceAccountOption interface {
	set(request *v1alpha1.DeleteServiceAccountRequest)
}

func (c *Client) DeleteServiceAccount(ctx context.Context, email string, opts ...DeleteServiceAccountOption) error {
	request := &v1alpha1.DeleteServiceAccountRequest{
		Name: fmt.Sprintf("projects/%s/serviceAccounts/%s", c.project, email),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	_, err := c.client.DeleteServiceAccount(ctx, request)
	return err
}
