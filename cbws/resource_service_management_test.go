package cbws

import (
	"fmt"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
)

// Test that a role resource can be created with permission
func TestAccSMRoleWithPermission_create(t *testing.T) {
	t.Parallel()

	serviceName := getTestServiceManagementServiceFromEnv()
	entity := "TestEntity"
	name := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	config := testAccSMRoleWithPermission_create(serviceName, entity, name)
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new role
			{
				Config: config,
				Check: resource.ComposeTestCheckFunc(
					testAccCheckSMRoleExists("cbws_service_management_role.acceptance", serviceName, name),
					testAccCheckSMPermissionExists("cbws_service_management_permission.acceptance", serviceName, entity, name),
				),
			},
		},
	})
}

func testAccSMRoleWithPermission_create(service, entity, role string) string {
	return fmt.Sprintf(`
resource "cbws_service_management_permission" "acceptance" {
  service      = "%s"
  type         = "%s"
  action       = "%s"
  description  = "This permission tests the Terraform provider"
}
resource "cbws_service_management_role" "acceptance" {
  service      = "%s"
  role         = "%s"
  display_name = "A role to test Terraform"
  description  = "This role tests the Terraform provider"
  permissions  = [
    "services/iam.cbws.xyz/permissions/ServiceAccount.create",
    cbws_service_management_permission.acceptance.name,
  ]
}
`, service, entity, role, service, role)
}
