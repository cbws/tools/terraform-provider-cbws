package cbws

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/cbws/terraform-provider-cbws/cbws/iam_backchannel"
)

// resourceServiceManagementPermission returns a *schema.Resource that allows a customer
// to declare a service management IAM permission resource.
func resourceServiceManagementPermission() *schema.Resource {
	return &schema.Resource{
		SchemaVersion: 1,

		Create: resourceServiceManagementPermissionCreate,
		Read:   resourceServiceManagementPermissionRead,
		Update: resourceServiceManagementPermissionUpdate,
		Delete: resourceServiceManagementPermissionDelete,
		Exists: resourceServiceManagementPermissionExists,

		Importer: &schema.ResourceImporter{
			State: resourceServiceManagementPermissionImportState,
		},

		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(5 * time.Minute),
			Read:   schema.DefaultTimeout(15 * time.Second),
			Delete: schema.DefaultTimeout(1 * time.Minute),
		},

		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The resource name of the permission",
			},
			"service": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateHostname(),
				Description:  "The service to which this permission will belong.",
			},
			"permission": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The permission name of the permission",
			},
			"type": {
				Type:        schema.TypeString,
				Required:    true,
				ForceNew:    true,
				Description: "The type of the permission. Changing this forces a new permission to be created.",
			},
			"action": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateName(),
				Description:  "The action of the permission. Changing this forces a new permission to be created.",
			},
			"description": {
				Type:        schema.TypeString,
				Optional:    true,
				Description: "The description of what this permission allows principals to do.",
			},
		},
	}
}

func resourceServiceManagementPermissionCreate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	service := d.Get("service").(string)
	entity := d.Get("type").(string)
	action := d.Get("action").(string)

	log.Printf("[DEBUG]: Creating new permission %s.%s", entity, action)

	ctx := context.Background()

	log.Printf("D: %+v", d)

	var opts []iam_backchannel.CreatePermissionOption
	if value := d.Get("description").(string); value != "" {
		opts = append(opts, iam_backchannel.WithCreatePermissionDescription(value))
	}

	permission, err := config.clientIAMBackchannel.CreatePermission(ctx, service, entity, action, opts...)
	if err != nil {
		return err
	}

	resourceServiceManagementPermissionData(permission, d)

	return nil
}

func resourceServiceManagementPermissionRead(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	service := d.Get("service").(string)
	permissionName := d.Get("permission").(string)

	log.Printf("[DEBUG]: Getting permission %s", permissionName)

	ctx := context.Background()
	permission, err := config.clientIAMBackchannel.GetPermission(ctx, service, permissionName)
	if err != nil {
		return err
	}

	resourceServiceManagementPermissionData(permission, d)

	return nil
}

func resourceServiceManagementPermissionUpdate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	service := d.Get("service").(string)
	permissionName := d.Get("permission").(string)

	log.Printf("[DEBUG]: Updating permission %s", permissionName)

	var opts []iam_backchannel.UpdatePermissionOption
	if d.HasChange("description") {
		opts = append(opts, iam_backchannel.WithUpdatePermissionDescription(d.Get("description").(string)))
	}

	ctx := context.Background()
	permission, err := config.clientIAMBackchannel.UpdatePermission(ctx, service, permissionName, opts...)
	if err != nil {
		return err
	}

	resourceServiceManagementPermissionData(permission, d)

	return nil
}

func resourceServiceManagementPermissionDelete(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	service := d.Get("service").(string)
	permissionName := d.Get("permission").(string)

	log.Printf("[DEBUG]: Deleting permission %s", permissionName)

	ctx := context.Background()
	err := config.clientIAMBackchannel.DeletePermission(ctx, service, permissionName)
	if err != nil {
		return err
	}

	d.SetId("")

	return nil
}

func resourceServiceManagementPermissionExists(d *schema.ResourceData, meta interface{}) (bool, error) {
	config := meta.(*Config)
	service := d.Get("service").(string)
	permissionName := d.Get("permission").(string)

	log.Printf("[DEBUG]: Checking permission existance %s", permissionName)

	ctx := context.Background()
	permission, err := config.clientIAMBackchannel.GetPermission(ctx, service, permissionName)
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.NotFound:
				return false, nil
			}
		}
		return false, err
	}

	resourceServiceManagementPermissionData(permission, d)

	return true, nil
}

func resourceServiceManagementPermissionImportState(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	config := meta.(*Config)
	split := strings.Split(d.Id(), "/")

	if len(split) != 4 || split[0] != "services" || split[2] != "permissions" {
		return nil, status.Error(codes.InvalidArgument, "id should be in format of services/iam.cbws.cloud/permissions/Entity.action")
	}

	service := split[1]
	permissionName := split[3]

	ctx := context.Background()
	permission, err := config.clientIAMBackchannel.GetPermission(ctx, service, permissionName)
	if err != nil {
		return nil, err
	}

	resourceServiceManagementPermissionData(permission, d)

	return []*schema.ResourceData{d}, nil
}

func resourceServiceManagementPermissionData(r iam_backchannel.Permission, d *schema.ResourceData) {
	d.SetId(r.Name)
	d.Set("name", r.Name)
	d.Set("permission", fmt.Sprintf("%s.%s", r.Type, r.Action))
	d.Set("type", r.Type)
	d.Set("action", r.Action)
	if r.Description != "" {
		d.Set("description", r.Description)
	} else {
		d.Set("description", nil)
	}
}
