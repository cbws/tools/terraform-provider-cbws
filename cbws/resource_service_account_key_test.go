package cbws

import (
	"fmt"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	"github.com/stretchr/testify/assert"
)

func TestServiceAccountEmailProject(t *testing.T) {
	project, err := serviceAccountEmailProject("test@some-projects.iam-sa.cbws.xyz")
	assert.NoError(t, err)
	assert.Equal(t, "some-projects", project)
}

// Test that a service account key resource can be created
func TestAccServiceAccountKey_create(t *testing.T) {
	t.Parallel()

	pid := getTestProjectFromEnv()
	name := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	email := fmt.Sprintf("%s@%s.iam-sa.cbws.xyz", name, pid)
	config := testAccServiceAccountKey_create(name)
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new role
			{
				Config: config,
				Check: resource.ComposeTestCheckFunc(
					testAccCheckServiceAccountKeyExists("cbws_service_account_key.acceptance", pid, email),
				),
			},
		},
	})
}

func TestAccServiceAccountKey_createDifferentProject(t *testing.T) {
	t.Parallel()

	org := getTestOrgFromEnv(t)
	pid := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	name := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	email := fmt.Sprintf("%s@%s.iam-sa.cbws.xyz", name, pid)
	config := testAccServiceAccountKey_createDifferentProject(pid, org, name)
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new role
			{
				Config: config,
				Check: resource.ComposeTestCheckFunc(
					testAccCheckServiceAccountKeyExists("cbws_service_account_key.acceptance", pid, email),
				),
			},
		},
	})
}

func testAccCheckServiceAccountKeyExists(r, pid, email string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[r]
		if !ok {
			return fmt.Errorf("not found: %s", r)
		}

		if rs.Primary.ID == "" {
			return fmt.Errorf("no ID is set")
		}

		uniqueId := rs.Primary.Attributes["unique_id"]
		expectedId := fmt.Sprintf("projects/%s/serviceAccounts/%s/keys/%s", pid, email, uniqueId)
		if rs.Primary.ID != expectedId {
			return fmt.Errorf("expected role id %q to match ID %q in state", expectedId, rs.Primary.ID)
		}

		return nil
	}
}

func testAccServiceAccountKey_create(name string) string {
	return fmt.Sprintf(`
resource "cbws_service_account" "acceptance" {
  account_id = "%s"
}

resource "cbws_service_account_key" "acceptance" {
  service_account = cbws_service_account.acceptance.email
}
`, name)
}

func testAccServiceAccountKey_createDifferentProject(pid, org, name string) string {
	return fmt.Sprintf(`
resource "cbws_project" "acceptance" {
  project      = "%s"
  organization = "%s"
}

resource "cbws_project_service" "acceptance" {
  project      = cbws_project.acceptance.project
  service      = "iam.cbws.xyz"
}

resource "cbws_service_account" "acceptance" {
  depends_on = [
    cbws_project_service.acceptance
  ]
  account_id = "%s"
  project    = cbws_project.acceptance.project
}

resource "cbws_service_account_key" "acceptance" {
  service_account = cbws_service_account.acceptance.email
}
`, pid, org, name)
}
