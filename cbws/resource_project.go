package cbws

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/cbws/go-cbws/cbws/projects/v1alpha1"
	"github.com/hashicorp/errwrap"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// resourceProject returns a *schema.Resource that allows a customer
// to declare a CBWS Project resource.
func resourceProject() *schema.Resource {
	return &schema.Resource{
		SchemaVersion: 1,

		Create: resourceProjectCreate,
		Read:   resourceProjectRead,
		Delete: resourceProjectDelete,

		Importer: &schema.ResourceImporter{
			State: resourceProjectImportState,
		},

		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(30 * time.Second),
			Read:   schema.DefaultTimeout(30 * time.Second),
			Delete: schema.DefaultTimeout(2 * time.Minute),
		},

		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: `The resource name of the project.`,
			},
			"project": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateProjectName(),
				Description:  `The name. Changing this forces a new project to be created.`,
			},
			"display_name": {
				Type:        schema.TypeString,
				Optional:    true,
				ForceNew:    true,
				Description: `The display name of the project.`,
			},
			"organization": {
				Type:        schema.TypeString,
				Optional:    true,
				Computed:    true,
				ForceNew:    true,
				Description: `The resource name of the organization this project belongs to.`,
			},
			"unique_id": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: `The UUID of the project.`,
			},
		},
	}
}

func resourceProjectCreate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	projectName := d.Get("project").(string)

	var err error
	log.Printf("[DEBUG]: Creating new project %s", projectName)

	organization, err := getOrganization(d, config)
	if err != nil {
		return err
	}
	parent := fmt.Sprintf("//organizations.cloudbear.nl/organizations/%s", organization)
	options := []v1alpha1.CreateProjectOption{}
	if _, ok := d.GetOk("display_name"); ok {
		options = append(options, v1alpha1.WithCreateProjectDisplayName(d.Get("display_name").(string)))
	}
	project, err := config.clientProjects.CreateProject(context.Background(), parent, projectName, options...)
	if err != nil {
		return fmt.Errorf("error creating project %s: %s. ", projectName, err)
	}

	d.SetId(project.Name)

	err = resourceProjectRead(d, meta)
	if err != nil {
		return err
	}

	return nil
}

func resourceProjectRead(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	projectName := d.Get("project").(string)

	p, err := config.clientProjects.GetProject(context.Background(), projectName)
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.NotFound:
				log.Printf("[WARN] Removing project %s because it's gone", d.Id())
				// The resource doesn't exist anymore
				d.SetId("")

				return nil
			}
		}

		return errwrap.Wrapf(fmt.Sprintf("Error when reading project %s: {{err}}", d.Id()), err)
	}

	_ = d.Set("name", p.Name)
	_ = d.Set("project", strings.Split(p.Name, "/")[1])
	_ = d.Set("display_name", p.DisplayName)
	_ = d.Set("organization", strings.Split(p.Organization, "/")[1])
	_ = d.Set("unique_id", p.UniqueId)

	return nil
}

func resourceProjectDelete(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	projectName := d.Get("project").(string)

	_, err := config.clientProjects.DeleteProject(context.Background(), projectName)
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.NotFound:
				log.Printf("[WARN] Removing project %s because it's gone", d.Id())
				// The resource doesn't exist anymore
				d.SetId("")

				return nil
			}
		}

		return errwrap.Wrapf(fmt.Sprintf("Error when deleting project %s: {{err}}", d.Id()), err)
	}

	d.SetId("")
	return nil
}

func resourceProjectImportState(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	err := resourceProjectRead(d, meta)
	if err != nil {
		return nil, err
	}

	return []*schema.ResourceData{d}, nil
}
