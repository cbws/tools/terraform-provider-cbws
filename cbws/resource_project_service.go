package cbws

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/cbws/go-pagination"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"

	"github.com/cbws/terraform-provider-cbws/cbws/service_usage"
)

// resourceProjectService returns a *schema.Resource that allows a customer
// to enable services on CBWS projects.
func resourceProjectService() *schema.Resource {
	return &schema.Resource{
		SchemaVersion: 1,

		Create: resourceProjectServiceCreate,
		Read:   resourceProjectServiceRead,
		Delete: resourceProjectServiceDelete,

		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(30 * time.Second),
			Read:   schema.DefaultTimeout(30 * time.Second),
			Delete: schema.DefaultTimeout(2 * time.Minute),
		},

		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: `The resource name of the project.`,
			},
			"project": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateProjectName(),
				Description:  `The name. Changing this forces a new project to be created.`,
			},
			"service": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateHostname(),
				Description:  `The name. Changing this forces a new project to be created.`,
			},
		},
	}
}

func resourceProjectServiceCreate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	project := d.Get("project").(string)
	serviceName := d.Get("service").(string)

	ctx := context.Background()
	paginator := config.clientServiceUsage.PaginateServices(project)
	services, err := pagination.Iterate(ctx, paginator)
	if err != nil {
		return err
	}

	var service service_usage.Service
	for _, edge := range services {
		if edge.Node.(service_usage.Service).Config.Name != serviceName {
			continue
		}

		service = edge.Node.(service_usage.Service)
	}
	// Service could not be found
	if service == nil {
		return fmt.Errorf("service %s could not be found", serviceName)
	}

	log.Printf("[DEBUG]: Enabling service %s on project %s", serviceName, project)

	err = config.clientServiceUsage.EnableService(ctx, project, serviceName)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("projects/%s/services/%s", project, serviceName)
	d.SetId(name)
	d.Set("name", name)

	return resourceProjectRead(d, meta)
}

func resourceProjectServiceRead(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	project := d.Get("project").(string)
	serviceName := d.Get("service").(string)

	ctx := context.Background()
	paginator := config.clientServiceUsage.PaginateServices(project)
	services, err := pagination.Iterate(ctx, paginator)
	if err != nil {
		return err
	}

	var service service_usage.Service
	for _, edge := range services {
		if edge.Node.(service_usage.Service).Config.Name != serviceName {
			continue
		}

		service = edge.Node.(service_usage.Service)
	}

	// Service could not be found
	if service == nil {
		d.SetId("")
		return nil
	}

	// Service has been disabled
	if service_usage.ServiceState(service.State).Disabled() {
		d.SetId("")
		return nil
	}

	return nil
}

func resourceProjectServiceDelete(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	project := d.Get("project").(string)
	service := d.Get("service").(string)

	ctx := context.Background()
	err := config.clientServiceUsage.DisableService(ctx, project, service)
	if err != nil {
		return err
	}

	d.SetId("")

	return nil
}
