package service_usage

import (
	"context"
	"fmt"
	"io"

	pb_service_usage "github.com/cbws/go-cbws-grpc/cbws/service-usage/v1alpha1"
	"github.com/cbws/go-cbws/client/grpc"
	"github.com/cbws/go-cbws/option"
	"github.com/cbws/go-pagination"
)

const endpoint = "serviceusage.cbws.xyz:443"

func NewClient(ctx context.Context, opts ...option.ClientOption) (*Client, error) {
	o := []option.ClientOption{
		option.WithEndpoint(endpoint),
		option.WithUserAgent("terraform-provider-cbws"),
	}
	o = append(o, opts...)

	conn, err := grpc.New(ctx, o...)
	if err != nil {
		return nil, err
	}

	return &Client{
		clientConn: conn,
		client:     pb_service_usage.NewServiceUsageServiceClient(conn),
	}, nil
}

type Client struct {
	client     pb_service_usage.ServiceUsageServiceClient
	clientConn io.Closer
}

func (c *Client) Close() error {
	return c.clientConn.Close()
}

type Service *pb_service_usage.Service

type ServiceState pb_service_usage.ServiceState

func (s ServiceState) Disabled() bool {
	return pb_service_usage.ServiceState(s) == pb_service_usage.ServiceState_SERVICE_STATE_DISABLED
}

func (c Client) PaginateServices(project string) pagination.Paginator {
	r := &pb_service_usage.ListServicesRequest{
		Parent: fmt.Sprintf("projects/%s", project),
	}

	return func(ctx context.Context, first, last *int, before, after pagination.Cursor) (*pagination.Connection, error) {
		connection := &pagination.Connection{
			PageInfo: pagination.PageInfo{},
		}

		response, err := c.client.ListServices(ctx, r)
		if err != nil {
			return nil, err
		}

		for _, service := range response.Services {
			connection.Edges = append(connection.Edges, pagination.Edge{
				Node: Service(service),
			})
		}
		connection.PageInfo.HasNextPage = false

		return connection, nil
	}
}

func (c *Client) EnableService(ctx context.Context, project, service string) error {
	_, err := c.client.EnableService(ctx, &pb_service_usage.EnableServiceRequest{
		Name: fmt.Sprintf("projects/%s/services/%s", project, service),
	})
	return err
}

func (c *Client) DisableService(ctx context.Context, project, service string) error {
	_, err := c.client.DisableService(ctx, &pb_service_usage.DisableServiceRequest{
		Name: fmt.Sprintf("projects/%s/services/%s", project, service),
	})
	return err
}
