package cbws

import (
	"context"
	"fmt"
	"log"
	"testing"

	"github.com/cbws/go-pagination"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"

	"github.com/cbws/terraform-provider-cbws/cbws/iam"
)

func init() {
	resource.AddTestSweepers("ServiceAccount", &resource.Sweeper{
		Name: "ServiceAccount",
		F:    testSweepServiceAccount,
	})
}

func testSweepServiceAccount(region string) error {
	config, err := sharedConfigForRegion(region)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error getting shared config for region: %s", err)
		return err
	}

	err = config.LoadAndValidate(context.Background())
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error loading: %s", err)
		return err
	}

	paginator := config.clientIAM.PaginateServiceAccounts()
	serviceAccounts, err := pagination.Iterate(context.Background(), paginator)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] Error listing service accounts: %s", err)
		return err
	}
	for _, edge := range serviceAccounts {
		serviceAccount := edge.Node.(iam.ServiceAccount)

		if !isSweepableTestResource(serviceAccount.Email) {
			log.Printf("[INFO][SWEEPER_LOG] Not sweeping non test service account: %s", serviceAccount.Email)

			continue
		}

		err = config.clientIAM.DeleteServiceAccount(context.Background(), serviceAccount.Email)
		if err != nil {
			return err
		}
	}

	return nil
}

// Test that a service account resource can be created
func TestAccServiceAccount_create(t *testing.T) {
	t.Parallel()

	pid := getTestProjectFromEnv()
	name := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	email := fmt.Sprintf("%s@%s.iam-sa.cbws.xyz", name, pid)
	config := testAccServiceAccount_create(name)
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new role
			{
				Config: config,
				Check: resource.ComposeTestCheckFunc(
					testAccCheckServiceAccountExists("cbws_service_account.acceptance", pid, email),
				),
			},
		},
	})
}

func TestAccServiceAccount_createDifferentProject(t *testing.T) {
	t.Parallel()

	org := getTestOrgFromEnv(t)
	pid := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	name := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	email := fmt.Sprintf("%s@%s.iam-sa.cbws.xyz", name, pid)
	config := testAccServiceAccount_createDifferentProject(pid, org, name)
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new role
			{
				Config: config,
				Check: resource.ComposeTestCheckFunc(
					testAccCheckServiceAccountExists("cbws_service_account.acceptance", pid, email),
				),
			},
		},
	})
}

func testAccCheckServiceAccountExists(r, pid, email string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[r]
		if !ok {
			return fmt.Errorf("not found: %s", r)
		}

		if rs.Primary.ID == "" {
			return fmt.Errorf("no ID is set")
		}

		expectedId := fmt.Sprintf("projects/%s/serviceAccounts/%s", pid, email)
		if rs.Primary.ID != expectedId {
			return fmt.Errorf("expected role id %q to match ID %q in state", expectedId, rs.Primary.ID)
		}

		return nil
	}
}

func testAccServiceAccount_create(name string) string {
	return fmt.Sprintf(`
resource "cbws_service_account" "acceptance" {
  account_id = "%s"
}
`, name)
}

func testAccServiceAccount_createDifferentProject(pid, org, name string) string {
	return fmt.Sprintf(`
resource "cbws_project" "acceptance" {
  project      = "%s"
  organization = "%s"
}

resource "cbws_project_service" "acceptance" {
  project      = cbws_project.acceptance.project
  service      = "iam.cbws.xyz"
}

resource "cbws_service_account" "acceptance" {
  depends_on = [
    cbws_project_service.acceptance
  ]
  account_id = "%s"
  project    = cbws_project.acceptance.project
}
`, pid, org, name)
}
