package cbws

import (
	"log"
	"os"

	pbaffinity "github.com/cbws/go-cbws-grpc/cbws/virtual-machines/affinity/v1alpha1"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func virtualMachineAffinity(d *schema.ResourceData) *pbaffinity.Affinity {
	if d.Get("affinity") == nil {
		return nil
	}

	affinity := &pbaffinity.Affinity{}

	affinityDataSet := d.Get("affinity").(*schema.Set)
	if affinityDataSet.Len() > 0 {
		affinityData := affinityDataSet.List()[0].(map[string]interface{})
		if nodeAffinityMapData, exists := affinityData["node_affinity"]; exists {
			nodeAffinitySet := nodeAffinityMapData.(*schema.Set)
			affinity.NodeAffinity = &pbaffinity.NodeAffinity{}
			if nodeAffinitySet.Len() > 0 {
				nodeAffinityData := nodeAffinitySet.List()[0].(map[string]interface{})
				if preferredMapData, exists := nodeAffinityData["preferred_during_scheduling_ignored_during_execution"]; exists {
					preferredSet := preferredMapData.(*schema.Set)
					if preferredSet.Len() > 0 {
						preferredData := preferredSet.List()[0].(map[string]interface{})
						terms := []*pbaffinity.PreferredSchedulingTerm{}
						if preferredData, exists := preferredData["preferred_scheduling_term"]; exists {
							term := &pbaffinity.PreferredSchedulingTerm{}
							preferredTerms := preferredData.([]interface{})
							for _, nodeTermData := range preferredTerms {
								nodeTermMap := nodeTermData.(map[string]interface{})
								if preference, exists := nodeTermMap["preference"]; exists {
									preferenceSet := preference.(*schema.Set)
									if preferenceSet.Len() > 0 {
										preferenceData := preferenceSet.List()[0].(map[string]interface{})
										term.Preference = &pbaffinity.NodeSelectorTerm{}
										if matchExpressionData, exists := preferenceData["match_expression"]; exists {
											matchExpressions := matchExpressionData.([]interface{})
											expressions := []*pbaffinity.LabelSelectorRequirement{}
											for _, matchExpressionData := range matchExpressions {
												expressions = append(expressions, virtualMachineAffinityLabelSelectorRequirement(matchExpressionData.(map[string]interface{})))
											}

											term.Preference.MatchExpressions = expressions
										}
									}
								}
								if weight, exists := nodeTermMap["weight"]; exists {
									term.Weight = int32(weight.(int))
								}
							}

							terms = append(terms, term)
						}
						affinity.NodeAffinity.PreferredDuringSchedulingIgnoredDuringExecution = terms
					}
				}
				if requiredMapData, exists := nodeAffinityData["required_during_scheduling_ignored_during_execution"]; exists {
					requiredSet := requiredMapData.(*schema.Set)
					affinity.NodeAffinity.RequiredDuringSchedulingIgnoredDuringExecution = &pbaffinity.NodeSelector{}
					if requiredSet.Len() > 0 {
						requiredData := requiredSet.List()[0].(map[string]interface{})
						if nodeTermData, exists := requiredData["node_selector_term"]; exists {
							nodeTerms := nodeTermData.([]interface{})
							terms := []*pbaffinity.NodeSelectorTerm{}
							for _, nodeTermData := range nodeTerms {
								nodeTermMap := nodeTermData.(map[string]interface{})
								term := &pbaffinity.NodeSelectorTerm{}
								if matchExpressionData, exists := nodeTermMap["match_expression"]; exists {
									matchExpressions := matchExpressionData.([]interface{})
									expressions := []*pbaffinity.LabelSelectorRequirement{}
									for _, matchExpressionData := range matchExpressions {
										expressions = append(expressions, virtualMachineAffinityLabelSelectorRequirement(matchExpressionData.(map[string]interface{})))
									}

									term.MatchExpressions = expressions
								}

								terms = append(terms, term)
							}
							affinity.NodeAffinity.RequiredDuringSchedulingIgnoredDuringExecution.NodeSelectorTerms = terms
						}
					}
				}
			}
		}
	}
	log.Printf("Affinity: %+v", affinity)
	os.Exit(1)

	return affinity
}

func virtualMachineAffinityLabelSelectorRequirement(selectorMap map[string]interface{}) *pbaffinity.LabelSelectorRequirement {
	expression := &pbaffinity.LabelSelectorRequirement{}
	if key, exists := selectorMap["key"]; exists {
		expression.Key = key.(string)
	}
	if operator, exists := selectorMap["operator"]; exists {
		switch operator.(string) {
		case "In":
			expression.Operator = pbaffinity.LabelSelectorRequirement_OPERATOR_IN
		case "NotIn":
			expression.Operator = pbaffinity.LabelSelectorRequirement_OPERATOR_NOT_IN
		case "Exists":
			expression.Operator = pbaffinity.LabelSelectorRequirement_OPERATOR_EXISTS
		case "DoesNotExist":
			expression.Operator = pbaffinity.LabelSelectorRequirement_OPERATOR_DOES_NOT_EXIST
		case "GreaterThan":
			expression.Operator = pbaffinity.LabelSelectorRequirement_OPERATOR_GREATER_THAN
		case "LessThan":
			expression.Operator = pbaffinity.LabelSelectorRequirement_OPERATOR_LESS_THAN
		}
	}
	if values, exists := selectorMap["values"]; exists {
		for _, value := range values.([]interface{}) {
			expression.Values = append(expression.Values, value.(string))
		}
	}

	return expression
}
