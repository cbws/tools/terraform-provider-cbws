package cbws

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	projects "github.com/cbws/go-cbws/cbws/projects/v1alpha1"
	pbvirtualmachines "github.com/cbws/go-cbws/cbws/virtual-machines/v1alpha1"
	cbwsoauth "github.com/cbws/go-cbws/oauth2"
	option2 "github.com/cbws/go-cbws/option"
	"github.com/hashicorp/go-cleanhttp"
	"github.com/hashicorp/terraform-plugin-sdk/helper/logging"
	"github.com/hashicorp/terraform-plugin-sdk/helper/pathorcontents"
	"github.com/hashicorp/terraform-plugin-sdk/httpclient"
	"golang.org/x/oauth2"

	"github.com/cbws/terraform-provider-cbws/cbws/iam"
	"github.com/cbws/terraform-provider-cbws/cbws/iam_backchannel"
	"github.com/cbws/terraform-provider-cbws/cbws/service_usage"
	"github.com/cbws/terraform-provider-cbws/version"
)

// Config is the configuration structure used to instantiate the CBWS
// provider.
type Config struct {
	Credentials         string
	AccessToken         string
	Organization        string
	Project             string
	Scopes              []string
	UserProjectOverride bool
	RequestTimeout      time.Duration
	PollInterval        time.Duration

	client           *http.Client
	context          context.Context
	terraformVersion string
	userAgent        string

	tokenSource oauth2.TokenSource

	clientProjects        *projects.Client
	clientVirtualMachines *pbvirtualmachines.Client
	clientIAMBackchannel  *iam_backchannel.Client
	clientServiceUsage    *service_usage.Client
	clientIAM             *iam.Client
}

var defaultClientScopes = []string{}

func (c *Config) LoadAndValidate(ctx context.Context) error {
	if len(c.Scopes) == 0 {
		c.Scopes = defaultClientScopes
	}

	tokenSource, err := c.getTokenSource(c.Scopes)
	if err != nil {
		return err
	}
	c.tokenSource = tokenSource

	tfUserAgent := httpclient.TerraformUserAgent(c.terraformVersion)
	providerVersion := fmt.Sprintf("terraform-provider-cbws/%s", version.ProviderVersion)
	userAgent := fmt.Sprintf("%s %s", tfUserAgent, providerVersion)

	c.context = ctx
	c.userAgent = userAgent

	cleanCtx := context.WithValue(ctx, oauth2.HTTPClient, cleanhttp.DefaultClient())

	// 1. OAUTH2 TRANSPORT/CLIENT - sets up proper auth headers
	client := oauth2.NewClient(cleanCtx, tokenSource)

	// 2. Logging Transport - ensure we log HTTP requests to GCP APIs.
	loggingTransport := logging.NewTransport("CBWS", client.Transport)

	// Set final transport value.
	client.Transport = loggingTransport

	// This timeout is a timeout per HTTP request, not per logical operation.
	client.Timeout = c.synchronousTimeout()

	log.Printf("[INFO] Instantiating projects client")
	c.clientProjects, err = projects.NewClient(ctx, option2.WithUserAgent(userAgent), option2.WithTokenSource(c.tokenSource))
	if err != nil {
		return err
	}

	log.Printf("[INFO] Instantiating virtual machines client")
	c.clientVirtualMachines, err = pbvirtualmachines.NewClient(ctx, c.Project, option2.WithUserAgent(userAgent), option2.WithTokenSource(c.tokenSource))
	if err != nil {
		return err
	}

	log.Printf("[INFO] Instantiating IAM backchannel client")
	c.clientIAMBackchannel, err = iam_backchannel.NewClient(ctx, option2.WithUserAgent(userAgent), option2.WithTokenSource(c.tokenSource))
	if err != nil {
		return err
	}

	log.Printf("[INFO] Instantiating service usage client")
	c.clientServiceUsage, err = service_usage.NewClient(ctx, option2.WithUserAgent(userAgent), option2.WithTokenSource(c.tokenSource))
	if err != nil {
		return err
	}

	log.Printf("[INFO] Instantiating IAM client")
	c.clientIAM, err = iam.NewClient(ctx, c.Project, option2.WithUserAgent(userAgent), option2.WithTokenSource(c.tokenSource))
	if err != nil {
		return err
	}

	c.PollInterval = 10 * time.Second

	return nil
}

func (c *Config) synchronousTimeout() time.Duration {
	if c.RequestTimeout == 0 {
		return 30 * time.Second
	}
	return c.RequestTimeout
}

func (c *Config) getTokenSource(clientScopes []string) (oauth2.TokenSource, error) {
	creds, err := c.GetCredentials(clientScopes)
	if err != nil {
		return nil, fmt.Errorf("%s", err)
	}

	return creds.TokenSource, nil
}

// staticTokenSource is used to be able to identify static token sources without reflection.
type staticTokenSource struct {
	oauth2.TokenSource
}

func (c *Config) GetCredentials(clientScopes []string) (*cbwsoauth.Credentials, error) {
	if c.AccessToken != "" {
		contents, _, err := pathorcontents.Read(c.AccessToken)
		if err != nil {
			return nil, fmt.Errorf("Error loading access token: %s", err)
		}

		log.Printf("[INFO] Authenticating using configured CBWS JSON 'access_token'...")
		log.Printf("[INFO]   -- Scopes: %s", clientScopes)
		token := &oauth2.Token{AccessToken: contents}

		return &cbwsoauth.Credentials{
			TokenSource: staticTokenSource{oauth2.StaticTokenSource(token)},
		}, nil
	}

	if c.Credentials != "" {
		contents, _, err := pathorcontents.Read(c.Credentials)
		if err != nil {
			return &cbwsoauth.Credentials{}, fmt.Errorf("error loading credentials: %s", err)
		}

		creds, err := cbwsoauth.NewFromJSON(c.context, []byte(contents), clientScopes)
		if err != nil {
			return nil, fmt.Errorf("unable to parse credentials from '%s': %s", contents, err)
		}

		log.Printf("[INFO] Authenticating using configured CBWS JSON 'credentials'...")
		log.Printf("[INFO]   -- Scopes: %s", clientScopes)
		return creds, nil
	}

	log.Printf("[INFO] Authenticating using DefaultClient...")
	log.Printf("[INFO]   -- Scopes: %s", clientScopes)

	creds, err := cbwsoauth.FindDefaultCredentials(context.Background(), clientScopes...)
	if err != nil {
		return nil, fmt.Errorf("could not get default CBWS crdentials: %s", err)
	}
	return creds, nil
}
