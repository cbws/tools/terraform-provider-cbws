package cbws

import (
	"context"
	"fmt"
	"log"
	"strings"
	"testing"

	"github.com/cbws/go-pagination"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"

	"github.com/cbws/terraform-provider-cbws/cbws/iam_backchannel"
)

func init() {
	resource.AddTestSweepers("ServiceManagementPermission", &resource.Sweeper{
		Name:         "ServiceManagementPermission",
		F:            testSweepServiceManagementPermission,
		Dependencies: []string{"ServiceManagementRole"},
	})
}

func testSweepServiceManagementPermission(region string) error {
	config, err := sharedConfigForRegion(region)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error getting shared config for region: %s", err)
		return err
	}

	err = config.LoadAndValidate(context.Background())
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error loading: %s", err)
		return err
	}

	serviceName := getTestServiceManagementServiceFromEnv()
	paginator := config.clientIAMBackchannel.PaginatePermissions(serviceName)
	permissions, err := pagination.Iterate(context.Background(), paginator)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] Error listing permissions: %s", err)
		return err
	}
	for _, edge := range permissions {
		permission := edge.Node.(iam_backchannel.Permission)

		resourceName := strings.Split(permission.Name, "/")[3]
		action := strings.Split(resourceName, ".")[1]
		if !isSweepableTestResource(action) {
			log.Printf("[INFO][SWEEPER_LOG] Not sweeping non test permission: %s", resourceName)

			continue
		}

		err = config.clientIAMBackchannel.DeletePermission(context.Background(), serviceName, resourceName)
		if err != nil {
			return err
		}
	}

	return nil
}

// Test that a permission resource can be created
func TestAccSMPermission_create(t *testing.T) {
	t.Parallel()

	serviceName := getTestServiceManagementServiceFromEnv()
	entity := "TestEntity"
	action := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	config := testAccSMPermission_create(serviceName, entity, action)
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new permission
			{
				Config: config,
				Check: resource.ComposeTestCheckFunc(
					testAccCheckSMPermissionExists("cbws_service_management_permission.acceptance", serviceName, entity, action),
				),
			},
		},
	})
}

func testAccCheckSMPermissionExists(r, service, entity, action string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[r]
		if !ok {
			return fmt.Errorf("not found: %s", r)
		}

		if rs.Primary.ID == "" {
			return fmt.Errorf("no ID is set")
		}

		expectedId := fmt.Sprintf("services/%s/permissions/%s.%s", service, entity, action)
		if rs.Primary.ID != expectedId {
			return fmt.Errorf("expected permission id %q to match ID %q in state", expectedId, rs.Primary.ID)
		}

		return nil
	}
}

func testAccSMPermission_create(service, entity, action string) string {
	return fmt.Sprintf(`
resource "cbws_service_management_permission" "acceptance" {
  service      = "%s"
  type         = "%s"
  action       = "%s"
  description  = "This permission tests the Terraform provider"
}
`, service, entity, action)
}
