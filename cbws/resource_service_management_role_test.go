package cbws

import (
	"context"
	"fmt"
	"log"
	"strings"
	"testing"

	"github.com/cbws/go-pagination"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"

	"github.com/cbws/terraform-provider-cbws/cbws/iam_backchannel"
)

func init() {
	resource.AddTestSweepers("ServiceManagementRole", &resource.Sweeper{
		Name: "ServiceManagementRole",
		F:    testSweepServiceManagementRole,
	})
}

var serviceManagementServiceEnvVars = []string{
	"CBWS_SERVICE_MANAGEMENT_SERVICE",
}

// testAccPreCheck ensures at least one of the service management service env variables is set.
func getTestServiceManagementServiceFromEnv() string {
	return multiEnvSearch(serviceManagementServiceEnvVars)
}

func testSweepServiceManagementRole(region string) error {
	config, err := sharedConfigForRegion(region)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error getting shared config for region: %s", err)
		return err
	}

	err = config.LoadAndValidate(context.Background())
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] error loading: %s", err)
		return err
	}

	serviceName := getTestServiceManagementServiceFromEnv()
	paginator := config.clientIAMBackchannel.PaginateRoles(serviceName)
	roles, err := pagination.Iterate(context.Background(), paginator)
	if err != nil {
		log.Printf("[INFO][SWEEPER_LOG] Error listing roles: %s", err)
		return err
	}
	for _, edge := range roles {
		role := edge.Node.(iam_backchannel.Role)

		resourceName := strings.Split(role.Name, "/")[3]
		if !isSweepableTestResource(resourceName) {
			log.Printf("[INFO][SWEEPER_LOG] Not sweeping non test role: %s", resourceName)

			continue
		}

		err = config.clientIAMBackchannel.DeleteRole(context.Background(), serviceName, resourceName)
		if err != nil {
			return err
		}
	}

	return nil
}

// Test that a role resource can be created
func TestAccSMRole_create(t *testing.T) {
	t.Parallel()

	serviceName := getTestServiceManagementServiceFromEnv()
	name := fmt.Sprintf("%s-%d", testPrefix, randInt(t))
	config := testAccSMRole_create(serviceName, name)
	vcrTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			// This step creates a new role
			{
				Config: config,
				Check: resource.ComposeTestCheckFunc(
					testAccCheckSMRoleExists("cbws_service_management_role.acceptance", serviceName, name),
				),
			},
		},
	})
}

func testAccCheckSMRoleExists(r, service, name string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[r]
		if !ok {
			return fmt.Errorf("not found: %s", r)
		}

		if rs.Primary.ID == "" {
			return fmt.Errorf("no ID is set")
		}

		expectedId := fmt.Sprintf("services/%s/roles/%s", service, name)
		if rs.Primary.ID != expectedId {
			return fmt.Errorf("expected role id %q to match ID %q in state", expectedId, rs.Primary.ID)
		}

		return nil
	}
}

func testAccSMRole_create(service, name string) string {
	return fmt.Sprintf(`
resource "cbws_service_management_role" "acceptance" {
  service      = "%s"
  role         = "%s"
  display_name = "A role to test Terraform"
  description  = "This role tests the Terraform provider"
  permissions  = [
    "services/iam.cbws.xyz/permissions/ServiceAccount.create",
  ]
}
`, service, name)
}
