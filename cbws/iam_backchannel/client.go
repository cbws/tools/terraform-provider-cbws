package iam_backchannel

import (
	pb_iam_backchannel "cbws.xyz/iam/api/grpc/service-backchannel/v1alpha1"
	"context"
	"fmt"
	"github.com/cbws/go-cbws/client/grpc"
	"github.com/cbws/go-cbws/option"
	"github.com/cbws/go-pagination"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
	"io"
)

const endpoint = "iam.cbws.cloud:443"

func NewClient(ctx context.Context, opts ...option.ClientOption) (*Client, error) {
	o := []option.ClientOption{
		option.WithEndpoint(endpoint),
		option.WithUserAgent("terraform-provider-cbws"),
	}
	o = append(o, opts...)

	conn, err := grpc.New(ctx, o...)
	if err != nil {
		return nil, err
	}

	return &Client{
		clientConn: conn,
		client:     pb_iam_backchannel.NewIAMServiceBackchannelServiceClient(conn),
	}, nil
}

type Client struct {
	client     pb_iam_backchannel.IAMServiceBackchannelServiceClient
	clientConn io.Closer
}

func (c *Client) Close() error {
	return c.clientConn.Close()
}

type Role *pb_iam_backchannel.Role

type ListRoleOption interface {
	set(r *pb_iam_backchannel.ListRolesRequest)
}

func (c Client) PaginateRoles(service string, opts ...ListRoleOption) pagination.Paginator {
	r := &pb_iam_backchannel.ListRolesRequest{
		Parent: fmt.Sprintf("services/%s", service),
	}
	for _, opt := range opts {
		opt.set(r)
	}

	return func(ctx context.Context, first, last *int, before, after pagination.Cursor) (*pagination.Connection, error) {
		connection := &pagination.Connection{
			PageInfo: pagination.PageInfo{},
		}
		if first != nil {
			r.PageSize = int32(*first)
		}
		if after != nil {
			r.PageToken = after.GetCursor()
		}

		response, err := c.client.ListRoles(ctx, r)
		if err != nil {
			return nil, err
		}
		for _, role := range response.Roles {
			connection.Edges = append(connection.Edges, pagination.Edge{
				Node: Role(role),
			})
		}
		connection.PageInfo.EndCursor = pagination.StringCursor(response.NextPageToken)
		connection.PageInfo.HasNextPage = response.NextPageToken != ""

		return connection, nil
	}
}

type createRoleDisplayName string

func (c createRoleDisplayName) set(request *pb_iam_backchannel.CreateRoleRequest) {
	request.Role.DisplayName = string(c)
}

func WithCreateRoleDisplayName(displayName string) CreateRoleOption {
	return createRoleDisplayName(displayName)
}

type createDescription string

func (c createDescription) set(request *pb_iam_backchannel.CreateRoleRequest) {
	request.Role.Description = string(c)
}

func WithCreateRoleDescription(Description string) CreateRoleOption {
	return createDescription(Description)
}

type CreateRoleOption interface {
	set(*pb_iam_backchannel.CreateRoleRequest)
}

func (c *Client) CreateRole(ctx context.Context, service, name string, permissions []string, opts ...CreateRoleOption) (Role, error) {
	request := &pb_iam_backchannel.CreateRoleRequest{
		Parent: "services/" + service,
		Role: &pb_iam_backchannel.Role{
			Role:        name,
			Permissions: permissions,
		},
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.CreateRole(ctx, request)
}

type GetRoleOption interface {
	set(request *pb_iam_backchannel.GetRoleRequest)
}

func (c *Client) GetRole(ctx context.Context, service string, name string, opts ...GetRoleOption) (Role, error) {
	request := &pb_iam_backchannel.GetRoleRequest{
		Name: fmt.Sprintf("services/%s/roles/%s", service, name),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.GetRole(ctx, request)
}

type updateRoleDisplayName string

func (c updateRoleDisplayName) set(request *pb_iam_backchannel.UpdateRoleRequest) {
	request.Role.DisplayName = string(c)
	request.UpdateMask.Paths = append(request.UpdateMask.Paths, "display_name")
}

func WithUpdateRoleDisplayName(displayName string) UpdateRoleOption {
	return updateRoleDisplayName(displayName)
}

type updateDescription string

func (c updateDescription) set(request *pb_iam_backchannel.UpdateRoleRequest) {
	request.Role.Description = string(c)
	request.UpdateMask.Paths = append(request.UpdateMask.Paths, "description")
}

func WithUpdateRoleDescription(Description string) UpdateRoleOption {
	return updateDescription(Description)
}

type UpdateRoleOption interface {
	set(request *pb_iam_backchannel.UpdateRoleRequest)
}

func (c *Client) UpdateRole(ctx context.Context, service string, name string, opts ...UpdateRoleOption) (Role, error) {
	request := &pb_iam_backchannel.UpdateRoleRequest{
		Role: &pb_iam_backchannel.Role{
			Name: fmt.Sprintf("services/%s/roles/%s", service, name),
		},
		UpdateMask: &fieldmaskpb.FieldMask{},
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.UpdateRole(ctx, request)
}

type DeleteRoleOption interface {
	set(request *pb_iam_backchannel.DeleteRoleRequest)
}

func (c *Client) DeleteRole(ctx context.Context, service string, name string, opts ...DeleteRoleOption) error {
	request := &pb_iam_backchannel.DeleteRoleRequest{
		Name: fmt.Sprintf("services/%s/roles/%s", service, name),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	_, err := c.client.DeleteRole(ctx, request)
	return err
}

type Permission *pb_iam_backchannel.Permission

type ListPermissionOption interface {
	set(r *pb_iam_backchannel.ListPermissionsRequest)
}

func (c Client) PaginatePermissions(service string, opts ...ListPermissionOption) pagination.Paginator {
	r := &pb_iam_backchannel.ListPermissionsRequest{
		Parent: fmt.Sprintf("services/%s", service),
	}
	for _, opt := range opts {
		opt.set(r)
	}

	return func(ctx context.Context, first, last *int, before, after pagination.Cursor) (*pagination.Connection, error) {
		connection := &pagination.Connection{
			PageInfo: pagination.PageInfo{},
		}
		if first != nil {
			r.PageSize = int32(*first)
		}
		if after != nil {
			r.PageToken = after.GetCursor()
		}

		response, err := c.client.ListPermissions(ctx, r)
		if err != nil {
			return nil, err
		}
		for _, role := range response.Permissions {
			connection.Edges = append(connection.Edges, pagination.Edge{
				Node: Permission(role),
			})
		}
		connection.PageInfo.EndCursor = pagination.StringCursor(response.NextPageToken)
		connection.PageInfo.HasNextPage = response.NextPageToken != ""

		return connection, nil
	}
}

type createPermissionDescription string

func (c createPermissionDescription) set(request *pb_iam_backchannel.CreatePermissionRequest) {
	request.Permission.Description = string(c)
}

func WithCreatePermissionDescription(Description string) CreatePermissionOption {
	return createPermissionDescription(Description)
}

type CreatePermissionOption interface {
	set(*pb_iam_backchannel.CreatePermissionRequest)
}

func (c *Client) CreatePermission(ctx context.Context, service, entity, action string, opts ...CreatePermissionOption) (Permission, error) {
	request := &pb_iam_backchannel.CreatePermissionRequest{
		Parent: "services/" + service,
		Permission: &pb_iam_backchannel.Permission{
			Type:   entity,
			Action: action,
		},
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.CreatePermission(ctx, request)
}

type GetPermissionOption interface {
	set(request *pb_iam_backchannel.GetPermissionRequest)
}

func (c *Client) GetPermission(ctx context.Context, service string, name string, opts ...GetPermissionOption) (Permission, error) {
	request := &pb_iam_backchannel.GetPermissionRequest{
		Name: fmt.Sprintf("services/%s/permissions/%s", service, name),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.GetPermission(ctx, request)
}

type updatePermissionDescription string

func (c updatePermissionDescription) set(request *pb_iam_backchannel.UpdatePermissionRequest) {
	request.Permission.Description = string(c)
	request.UpdateMask.Paths = append(request.UpdateMask.Paths, "description")
}

func WithUpdatePermissionDescription(Description string) UpdatePermissionOption {
	return updatePermissionDescription(Description)
}

type UpdatePermissionOption interface {
	set(request *pb_iam_backchannel.UpdatePermissionRequest)
}

func (c *Client) UpdatePermission(ctx context.Context, service string, name string, opts ...UpdatePermissionOption) (Permission, error) {
	request := &pb_iam_backchannel.UpdatePermissionRequest{
		Permission: &pb_iam_backchannel.Permission{
			Name: fmt.Sprintf("services/%s/permissions/%s", service, name),
		},
		UpdateMask: &fieldmaskpb.FieldMask{},
	}
	for _, opt := range opts {
		opt.set(request)
	}

	return c.client.UpdatePermission(ctx, request)
}

type DeletePermissionOption interface {
	set(request *pb_iam_backchannel.DeletePermissionRequest)
}

func (c *Client) DeletePermission(ctx context.Context, service string, name string, opts ...DeletePermissionOption) error {
	request := &pb_iam_backchannel.DeletePermissionRequest{
		Name: fmt.Sprintf("services/%s/permissions/%s", service, name),
	}
	for _, opt := range opts {
		opt.set(request)
	}

	_, err := c.client.DeletePermission(ctx, request)
	return err
}
