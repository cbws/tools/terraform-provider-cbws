package cbws

import (
	"context"
	"log"
	"strings"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/cbws/terraform-provider-cbws/cbws/iam"
)

// resourceServiceAccount returns a *schema.Resource that allows a customer
// to declare a service account resource.
func resourceServiceAccount() *schema.Resource {
	return &schema.Resource{
		SchemaVersion: 1,

		Create: resourceServiceAccountCreate,
		Read:   resourceServiceAccountRead,
		Update: resourceServiceAccountUpdate,
		Delete: resourceServiceAccountDelete,

		Importer: &schema.ResourceImporter{
			State: resourceServiceAccountImportState,
		},

		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(5 * time.Minute),
			Read:   schema.DefaultTimeout(15 * time.Second),
			Delete: schema.DefaultTimeout(1 * time.Minute),
		},

		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The resource name of the service account.",
			},
			"email": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The email address of the service account.",
			},
			"unique_id": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The unique id of the service account.",
			},
			"account_id": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateRFC1035Name(6, 30),
				Description:  "The account id that used to generate the service account email address.",
			},
			"display_name": {
				Type:        schema.TypeString,
				Optional:    true,
				Description: "The name to show to users.",
			},
			"project": {
				Type:        schema.TypeString,
				Optional:    true,
				Computed:    true,
				ForceNew:    true,
				Description: "The project the service account will be created in.",
			},
		},
	}
}

func resourceServiceAccountCreate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	accountID := d.Get("account_id").(string)

	log.Printf("[DEBUG]: Creating new service account %s", accountID)

	ctx := context.Background()

	var opts []iam.CreateServiceAccountOption
	if value := d.Get("display_name").(string); value != "" {
		opts = append(opts, iam.WithCreateServiceAccountDisplayName(value))
	}
	if value := d.Get("project").(string); value != "" {
		opts = append(opts, iam.WithCreateServiceAccountProject(value))
	}

	serviceAccount, err := config.clientIAM.CreateServiceAccount(ctx, accountID, opts...)
	if err != nil {
		return err
	}

	d.SetId(serviceAccount.Name)
	d.Set("email", serviceAccount.Email)

	return resourceServiceAccountRead(d, meta)
}

func resourceServiceAccountRead(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	email := d.Get("email").(string)

	log.Printf("[DEBUG]: Getting service account %s", email)

	opts := []iam.GetServiceAccountOption{}
	if value := d.Get("project").(string); value != "" {
		opts = append(opts, iam.WithGetServiceAccountProject(value))
	}

	ctx := context.Background()
	serviceAccount, err := config.clientIAM.GetServiceAccount(ctx, email, opts...)
	if err != nil {
		return err
	}

	d.SetId(serviceAccount.Name)
	d.Set("name", serviceAccount.Name)
	d.Set("unique_id", serviceAccount.UniqueId)
	d.Set("email", serviceAccount.Email)
	d.Set("display_name", serviceAccount.DisplayName)
	d.Set("project", serviceAccount.ProjectId)

	return nil
}

func resourceServiceAccountUpdate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	email := d.Get("email").(string)

	log.Printf("[DEBUG]: Updating service account %s", email)

	var opts []iam.UpdateServiceAccountOption
	if d.HasChange("display_name") {
		opts = append(opts, iam.WithUpdateServiceAccountDisplayName(d.Get("display_name").(string)))
	}
	if value := d.Get("project").(string); value != "" {
		opts = append(opts, iam.WithUpdateServiceAccountProject(value))
	}

	ctx := context.Background()
	_, err := config.clientIAM.UpdateServiceAccount(ctx, email, opts...)
	if err != nil {
		return err
	}

	return resourceServiceAccountRead(d, meta)
}

func resourceServiceAccountDelete(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	email := d.Get("email").(string)

	log.Printf("[DEBUG]: Deleting service account %s", email)

	opts := []iam.DeleteServiceAccountOption{}
	if value := d.Get("project").(string); value != "" {
		opts = append(opts, iam.WithDeleteServiceAccountProject(value))
	}

	ctx := context.Background()
	err := config.clientIAM.DeleteServiceAccount(ctx, email, opts...)
	if err != nil {
		return err
	}

	d.SetId("")

	return nil
}

func resourceServiceAccountImportState(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	split := strings.Split(d.Id(), "/")

	if len(split) != 4 || split[0] != "projects" || split[2] != "serviceAccounts" {
		return nil, status.Error(codes.InvalidArgument, "id should be in format of projects/project/serviceAccount/email")
	}

	project := split[1]
	email := split[3]

	d.Set("project", project)
	d.Set("email", email)

	err := resourceServiceAccountRead(d, meta)
	if err != nil {
		return nil, err
	}

	return []*schema.ResourceData{d}, nil
}
