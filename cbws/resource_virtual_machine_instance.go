package cbws

import (
	"context"
	"fmt"
	"log"
	"time"

	pbvirtualmachines2 "github.com/cbws/go-cbws-grpc/cbws/virtual-machines/v1alpha1"
	v1alpha12 "github.com/cbws/go-cbws-grpc/cbws/virtual-machines/v1alpha1"
	pbvirtualmachines "github.com/cbws/go-cbws/cbws/virtual-machines/v1alpha1"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// resourceVirtualMachinesInstance returns a *schema.Resource that allows a customer
// to declare a CBWS VirtualMachinesInstance resource.
func resourceVirtualMachinesInstance() *schema.Resource {
	return &schema.Resource{
		SchemaVersion: 1,

		Create: resourceVirtualMachinesInstanceCreate,
		Read:   resourceVirtualMachinesInstanceRead,
		Update: resourceVirtualMachinesInstanceUpdate,
		Delete: resourceVirtualMachinesInstanceDelete,
		Exists: resourceVirtualMachinesInstanceExists,

		Importer: &schema.ResourceImporter{
			State: resourceVirtualMachinesInstanceImportState,
		},

		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(5 * time.Minute),
			Read:   schema.DefaultTimeout(15 * time.Second),
			Delete: schema.DefaultTimeout(1 * time.Minute),
		},

		Schema: map[string]*schema.Schema{
			"name": {
				Type:         schema.TypeString,
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validateName(),
				Description:  `The name. Changing this forces a new instance to be created.`,
			},
			"hostname": {
				Type:        schema.TypeString,
				Required:    true,
				Description: `The hostname of the instance.`,
			},
			"cpu": {
				Type:        schema.TypeInt,
				Required:    true,
				Description: `The amount of CPU of the instance.`,
			},
			"memory": {
				Type:        schema.TypeInt,
				Required:    true,
				Description: `The amount of memory of the instance.`,
			},
			"root_disk": {
				Type:        schema.TypeInt,
				Required:    true,
				Description: `The amount of root disk of the instance.`,
			},
			"address": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: `The address the instance.`,
			},
			"ssh_keys": {
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type:        schema.TypeString,
					Description: `A SSH key for the instance.`,
				},
				Description: `The address the instance.`,
			},
			"affinity": {
				Type:        schema.TypeSet,
				MaxItems:    1,
				Optional:    true,
				Description: `The address the instance.`,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"node_affinity": {
							Type:     schema.TypeSet,
							MaxItems: 1,
							Optional: true,
							Elem: &schema.Resource{
								Schema: map[string]*schema.Schema{
									"preferred_during_scheduling_ignored_during_execution": {
										Type:        schema.TypeSet,
										MaxItems:    1,
										Optional:    true,
										Description: "The scheduler will prefer to schedule instances to nodes that satisfy the affinity expressions specified by this field, but it may choose a node that violates one or more of the expressions. The node that is most preferred is the one with the greatest sum of weights, i.e. for each node that meets all of the scheduling requirements (resource request, requiredDuringScheduling affinity expressions, etc.), compute a sum by iterating through the elements of this field and adding \"weight\" to the sum if the node matches the corresponding matchExpressions; the node(s) with the highest sum are the most preferred",
										Elem: &schema.Resource{
											Schema: map[string]*schema.Schema{
												"preferred_scheduling_term": {
													Type:        schema.TypeList,
													Optional:    true,
													Description: "A list of node selector terms. The terms are ORed.",
													Elem: &schema.Resource{
														Schema: map[string]*schema.Schema{
															"preference": {
																Type:        schema.TypeSet,
																Required:    true,
																Description: "A node selector term, associated with the corresponding weight",
																Elem: &schema.Resource{
																	Schema: map[string]*schema.Schema{
																		"match_expression": {
																			Type:        schema.TypeList,
																			Optional:    true,
																			Description: "A list of node selector requirements by node's labels",
																			Elem: &schema.Resource{
																				Schema: map[string]*schema.Schema{
																					"key": {
																						Type:        schema.TypeString,
																						Required:    true,
																						Description: `The label key that the selector applies to`,
																					},
																					"operator": {
																						Type:        schema.TypeString,
																						Required:    true,
																						Description: `Represents a key's relationship to a set of values. Valid operators are In, NotIn, Exists, DoesNotExist. GreaterThan, and LessThan`,
																					},
																					"values": {
																						Type:     schema.TypeList,
																						Optional: true,
																						Elem: &schema.Schema{
																							Type: schema.TypeString,
																						},
																						Description: `An array of string values. If the operator is In or NotIn, the values array must be non-empty. If the operator is Exists or DoesNotExist, the values array must be empty. If the operator is Gt or Lt, the values array must have a single element, which will be interpreted as an integer.`,
																					},
																				},
																			},
																		},
																	},
																},
															},
															"weight": {
																Type:        schema.TypeInt,
																Optional:    true,
																Description: `Weight associated with matching the corresponding nodeSelectorTerm, in the range 1-100`,
															},
														},
													},
												},
											},
										},
									},
									"required_during_scheduling_ignored_during_execution": {
										Type:        schema.TypeSet,
										MaxItems:    1,
										Optional:    true,
										Description: "If the affinity requirements specified by this field are not met at scheduling time, the instance will not be scheduled onto the node. If the affinity requirements specified by this field cease to be met at some point during instance execution (e.g. due to an update), the system may or may not try to eventually evict the instance from its node",
										Elem: &schema.Resource{
											Schema: map[string]*schema.Schema{
												"node_selector_term": {
													Type:        schema.TypeList,
													Optional:    true,
													Description: "A list of node selector terms. The terms are ORed.",
													Elem: &schema.Resource{
														Schema: map[string]*schema.Schema{
															"match_expression": {
																Type:        schema.TypeList,
																Optional:    true,
																Description: "A list of node selector requirements by node's labels",
																Elem: &schema.Resource{
																	Schema: map[string]*schema.Schema{
																		"key": {
																			Type:        schema.TypeString,
																			Required:    true,
																			Description: `The label key that the selector applies to`,
																		},
																		"operator": {
																			Type:     schema.TypeString,
																			Required: true,
																			ValidateFunc: func(v interface{}, k string) (ws []string, errors []error) {
																				value := v.(string)
																				if value != "In" && value != "NotIn" && value != "Exists" && value != "DoesNotExist" && value != "GreaterThan" && value != "LessThan" {
																					errors = append(errors, fmt.Errorf("operator %s is invalid, should be one of In, NotIn, Exists, DoesNotExist, GreaterThan or LessThan", value))
																				}

																				return
																			},
																			Description: `Represents a key's relationship to a set of values. Valid operators are In, NotIn, Exists, DoesNotExist. GreaterThan, and LessThan`,
																		},
																		"values": {
																			Type:     schema.TypeList,
																			Optional: true,
																			Elem: &schema.Schema{
																				Type: schema.TypeString,
																			},
																			Description: `An array of string values. If the operator is In or NotIn, the values array must be non-empty. If the operator is Exists or DoesNotExist, the values array must be empty. If the operator is Gt or Lt, the values array must have a single element, which will be interpreted as an integer.`,
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
			"labels": {
				Type:        schema.TypeMap,
				Optional:    true,
				Description: "A list of labels useful for filtering, affinities and load balancing",
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
		},
	}
}

func resourceVirtualMachinesInstanceCreate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	name := d.Get("name").(string)

	var err error
	log.Printf("[DEBUG]: Creating new virtual machine instance %s", name)

	sshKeys := []string{}
	for _, sshKey := range d.Get("ssh_keys").([]interface{}) {
		sshKeys = append(sshKeys, sshKey.(string))
	}
	labels := make(map[string]string)
	for key, value := range d.Get("labels").(map[string]interface{}) {
		labels[key] = value.(string)
	}
	instance := &pbvirtualmachines2.Instance{
		Hostname: d.Get("hostname").(string),
		Cpu:      int64(d.Get("cpu").(int)),
		Memory:   int64(d.Get("memory").(int)),
		RootDisk: int64(d.Get("root_disk").(int)),
		SshKeys:  sshKeys,
		Labels:   labels,
		Affinity: virtualMachineAffinity(d),
	}

	ctx := context.Background()
	_, err = config.clientVirtualMachines.Create(ctx, name, instance)
	if err != nil {
		return fmt.Errorf("error creating instance: %+v", err)
	}

	//cmc := make(chan *v1alpha12.CreateInstanceMetadata)
	//go func() {
	//	for {
	//		select {
	//		case md, ok := <-cmc:
	//			if !ok {
	//				return
	//			}
	//
	//			log.Printf("%s - Metadata: %+v", name, md)
	//		}
	//	}
	//}()
	//instance, st, err := operationHandler.Wait(
	//	context.Background(),
	//pbvirtualmachines.WithInstanceMetadataChannel(cmc),
	//)
	//if err != nil {
	//	return fmt.Errorf("error wait till instance created: %+v", err)
	//}
	//if st != nil {
	//	return fmt.Errorf("error creating instance: %+v", st)
	//}

	//virtualMachineInstanceData(instance, d)

	return nil
}

func resourceVirtualMachinesInstanceRead(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	name := d.Get("name").(string)

	var err error
	log.Printf("[DEBUG]: Getting virtual machine instance %s", name)

	ctx := context.Background()
	instance, err := config.clientVirtualMachines.Get(ctx, name, pbvirtualmachines.WithGetFieldMask([]string{
		"hostname",
		"cpu",
		"memory",
		"root_disk",
		"ssh_keys",
		"labels",
	}))
	if err != nil {
		return fmt.Errorf("error getting instance: %+v", err)
	}

	virtualMachineInstanceData(instance, d)

	return nil
}

func resourceVirtualMachinesInstanceUpdate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	name := d.Get("name").(string)

	var err error
	log.Printf("[DEBUG]: Updating new virtual machine instance %s", name)

	instance := &pbvirtualmachines2.Instance{}
	fields := []string{}

	if d.HasChange("hostname") {
		instance.Hostname = d.Get("hostname").(string)
		fields = append(fields, "hostname")
	}
	if d.HasChange("cpu") {
		instance.Cpu = int64(d.Get("cpu").(int))
		fields = append(fields, "cpu")
	}
	if d.HasChange("memory") {
		instance.Memory = int64(d.Get("memory").(int))
		fields = append(fields, "memory")
	}
	if d.HasChange("root_disk") {
		instance.RootDisk = int64(d.Get("root_disk").(int))
		fields = append(fields, "root_disk")
	}
	if d.HasChange("ssh_keys") {
		sshKeys := []string{}
		for _, sshKey := range d.Get("ssh_keys").([]interface{}) {
			sshKeys = append(sshKeys, sshKey.(string))
		}
		instance.SshKeys = sshKeys
		fields = append(fields, "ssh_keys")
	}
	if d.HasChange("labels") {
		labels := make(map[string]string)
		for key, value := range d.Get("labels").(map[string]interface{}) {
			labels[key] = value.(string)
		}
		instance.Labels = labels
		fields = append(fields, "labels")
	}

	ctx := context.Background()
	_, err = config.clientVirtualMachines.Update(ctx, name, instance, []string{})
	if err != nil {
		return fmt.Errorf("error updating instance: %+v", err)
	}

	//umc := make(chan *v1alpha12.UpdateInstanceMetadata)
	//go func() {
	//	for {
	//		select {
	//		case md, ok := <-umc:
	//			if !ok {
	//				return
	//			}
	//
	//			log.Printf("Metadata: %+v", md)
	//		}
	//	}
	//}()
	//instance, status, err := operationHandler.Wait(
	//	context.Background(),
	//pbvirtualmachines.WithUpdateInstanceMetadataChannel(umc),
	//)
	//if err != nil {
	//	return fmt.Errorf("error wait till instance updated: %+v", err)
	//}
	//if status != nil {
	//	return fmt.Errorf("error updating instance: %+v", status)
	//}

	//virtualMachineInstanceData(instance, d)

	return nil
}

func resourceVirtualMachinesInstanceDelete(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	name := d.Get("name").(string)

	var err error
	log.Printf("[DEBUG]: Deleting virtual machine instance %s", name)

	ctx := context.Background()
	operationHandler, err := config.clientVirtualMachines.Delete(ctx, name)
	if err != nil {
		return fmt.Errorf("error deleting instance: %+v", err)
	}

	dmc := make(chan *v1alpha12.DeleteInstanceMetadata)
	go func() {
		for {
			select {
			case md, ok := <-dmc:
				if !ok {
					return
				}

				log.Printf("Metadata: %+v", md)
			}
		}
	}()
	status, err := operationHandler.Wait(context.Background())
	if err != nil {
		return fmt.Errorf("error wait till instance deleted: %+v", err)
	}
	if status != nil {
		return fmt.Errorf("error deleting instance: %+v", status)
	}

	d.SetId("")

	return nil
}

func resourceVirtualMachinesInstanceExists(d *schema.ResourceData, meta interface{}) (bool, error) {
	config := meta.(*Config)
	name := d.Get("name").(string)

	var err error
	log.Printf("[DEBUG]: Checking virtual machine instance %s", name)

	ctx := context.Background()
	instance, err := config.clientVirtualMachines.Get(ctx, name, pbvirtualmachines.WithGetFieldMask([]string{
		"hostname",
		"cpu",
		"memory",
		"root_disk",
		"ssh_keys",
		"labels",
	}))
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.NotFound:
				return false, nil
			}
		}
		return false, fmt.Errorf("error checking instance: %+v", err)
	}

	virtualMachineInstanceData(instance, d)

	return true, nil
}

func resourceVirtualMachinesInstanceImportState(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	config := meta.(*Config)

	ctx := context.Background()
	instance, err := config.clientVirtualMachines.Get(ctx, d.Id(), pbvirtualmachines.WithGetFieldMask([]string{
		"hostname",
		"cpu",
		"memory",
		"root_disk",
		"ssh_keys",
		"labels",
	}))
	if err != nil {
		return nil, fmt.Errorf("error getting instance: %+v", err)
	}

	d.Set("name", d.Id())
	virtualMachineInstanceData(instance, d)
	err = resourceVirtualMachinesInstanceRead(d, meta)
	if err != nil {
		return nil, err
	}

	return []*schema.ResourceData{d}, nil
}

func virtualMachineInstanceData(i pbvirtualmachines.Instance, d *schema.ResourceData) {
	d.SetId(i.Name)
	d.Set("hostname", i.Hostname)
	d.Set("cpu", i.Cpu)
	d.Set("memory", i.Memory)
	d.Set("root_disk", i.RootDisk)
	d.Set("address", i.Address)
	d.Set("ssh_keys", i.SshKeys)
	d.Set("labels", i.Labels)

	// Initialize the connection info
	d.SetConnInfo(map[string]string{
		"type": "ssh",
		"host": fmt.Sprintf("[%s]", i.Address),
		"user": "ubuntu",
	})
}
