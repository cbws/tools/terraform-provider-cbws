package cbws

import (
	"context"
	"fmt"
	"log"
	"net/mail"
	"strings"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/helper/validation"

	"github.com/cbws/terraform-provider-cbws/cbws/iam"
)

// resourceServiceAccountKey returns a *schema.Resource that allows a customer
// to declare a service account key resource.
func resourceServiceAccountKey() *schema.Resource {
	return &schema.Resource{
		SchemaVersion: 1,

		Create: resourceServiceAccountKeyCreate,
		Read:   resourceServiceAccountKeyRead,
		Delete: resourceServiceAccountKeyDelete,

		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(5 * time.Minute),
			Read:   schema.DefaultTimeout(15 * time.Second),
			Delete: schema.DefaultTimeout(1 * time.Minute),
		},

		Schema: map[string]*schema.Schema{
			// Required
			"service_account": {
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
				ValidateFunc: validation.Any(
					validateRFC1035Name(6, 30),
					func(v interface{}, k string) (s []string, errors []error) {
						value := v.(string)
						_, err := mail.ParseAddress(value)
						if err != nil {
							errors = append(errors, err)
						}

						return
					},
				),
				Description: `The ID of the parent service account of the key. This can be a string in the format {ACCOUNT} or projects/{PROJECT_ID}/serviceAccounts/{ACCOUNT}, where {ACCOUNT} is the email address or unique id of the service account. If the {ACCOUNT} syntax is used, the project will be inferred from the provider's configuration.`,
			},
			// Optional
			"key_algorithm": {
				Type:         schema.TypeString,
				Default:      "KEY_ALG_RSA_2048",
				Optional:     true,
				ForceNew:     true,
				ValidateFunc: validation.StringInSlice([]string{"KEY_ALG_UNSPECIFIED", "KEY_ALG_RSA_1024", "KEY_ALG_RSA_2048"}, false),
				Description:  `The algorithm used to generate the key, used only on create. KEY_ALG_RSA_2048 is the default algorithm. Valid values are: "KEY_ALG_RSA_1024", "KEY_ALG_RSA_2048".`,
			},
			"private_key_type": {
				Type:         schema.TypeString,
				Default:      "TYPE_CREDENTIALS_FILE",
				Optional:     true,
				ForceNew:     true,
				ValidateFunc: validation.StringInSlice([]string{"TYPE_UNSPECIFIED", "TYPE_PKCS12_FILE", "TYPE_CREDENTIALS_FILE"}, false),
			},
			"public_key_type": {
				Type:         schema.TypeString,
				Default:      "TYPE_X509_PEM_FILE",
				Optional:     true,
				ForceNew:     true,
				ValidateFunc: validation.StringInSlice([]string{"TYPE_NONE", "TYPE_X509_PEM_FILE", "TYPE_RAW_PUBLIC_KEY"}, false),
			},
			"public_key_data": {
				Type:          schema.TypeString,
				Optional:      true,
				ForceNew:      true,
				ConflictsWith: []string{"key_algorithm", "private_key_type"},
				Description:   `A field that allows clients to upload their own public key. If set, use this public key data to create a service account key for given service account. Please note, the expected format for this field is a base64 encoded X509_PEM.`,
			},
			"keepers": {
				Description: "Arbitrary map of values that, when changed, will trigger recreation of resource.",
				Type:        schema.TypeMap,
				Optional:    true,
				ForceNew:    true,
			},
			// Computed
			"name": {
				Type:        schema.TypeString,
				Computed:    true,
				ForceNew:    true,
				Description: `The name used for this key pair`,
			},
			"unique_id": {
				Type:        schema.TypeString,
				Computed:    true,
				ForceNew:    true,
				Description: `The unique id of this key pair`,
			},
			"public_key": {
				Type:        schema.TypeString,
				Computed:    true,
				ForceNew:    true,
				Description: `The public key, base64 encoded`,
			},
			"private_key": {
				Type:        schema.TypeString,
				Computed:    true,
				Sensitive:   true,
				Description: `The private key in JSON format, base64 encoded. This is what you normally get as a file when creating service account keys through the CLI or web console. This is only populated when creating a new key.`,
			},
			"valid_after": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: `The key can be used after this timestamp. A timestamp in RFC3339 UTC "Zulu" format, accurate to nanoseconds. Example: "2014-10-02T15:01:23.045123456Z".`,
			},
			"valid_before": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: `The key can be used before this timestamp. A timestamp in RFC3339 UTC "Zulu" format, accurate to nanoseconds. Example: "2014-10-02T15:01:23.045123456Z".`,
			},
		},
	}
}

func serviceAccountEmail(serviceAccount string, d TerraformResourceData, config *Config) (string, error) {
	// If the service account id is an email
	if strings.Contains(serviceAccount, "@") {
		return serviceAccount, nil
	}

	// Get the project from the resource or fallback to the project
	// in the provider configuration
	project, err := getProject(d, config)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s@%s.iam-sa.cbws.xyz", serviceAccount, project), nil
}

func serviceAccountEmailProject(serviceAccount string) (string, error) {
	return strings.Split(strings.Split(serviceAccount, "@")[1], ".")[0], nil
}

func resourceServiceAccountKeyCreate(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)

	serviceAccountEmail, err := serviceAccountEmail(d.Get("service_account").(string), d, config)
	if err != nil {
		return err
	}

	project, err := serviceAccountEmailProject(serviceAccountEmail)
	if err != nil {
		return err
	}

	log.Printf("[DEBUG]: Creating new service account key on %s", serviceAccountEmail)

	ctx := context.Background()

	opts := []iam.CreateServiceAccountKeyOption{
		iam.WithCreateServiceAccountKeyProject(project),
	}

	serviceAccountKey, err := config.clientIAM.CreateServiceAccountKey(ctx, serviceAccountEmail, opts...)
	if err != nil {
		return err
	}

	d.SetId(serviceAccountKey.Name)
	d.Set("name", serviceAccountKey.Name)
	d.Set("unique_id", strings.Split(serviceAccountKey.Name, "/")[5])

	// Data only available on create.
	if err := d.Set("valid_after", serviceAccountKey.ValidAfterTime.AsTime().Format(time.RFC3339)); err != nil {
		return fmt.Errorf("error setting valid_after: %s", err)
	}
	if err := d.Set("valid_before", serviceAccountKey.ValidBeforeTime.AsTime().Format(time.RFC3339)); err != nil {
		return fmt.Errorf("error setting valid_before: %s", err)
	}
	if err := d.Set("private_key", string(serviceAccountKey.PrivateKeyData)); err != nil {
		return fmt.Errorf("error setting private_key: %s", err)
	}

	return resourceServiceAccountKeyRead(d, meta)
}

func resourceServiceAccountKeyRead(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	uniqueId := d.Get("unique_id").(string)
	serviceAccountEmail, err := serviceAccountEmail(d.Get("service_account").(string), d, config)
	if err != nil {
		return err
	}

	project, err := serviceAccountEmailProject(serviceAccountEmail)
	if err != nil {
		return err
	}

	log.Printf("[DEBUG]: Getting service account key %s of %s", uniqueId, serviceAccountEmail)

	opts := []iam.GetServiceAccountKeyOption{
		iam.WithGetServiceAccountKeyProject(project),
	}

	ctx := context.Background()
	serviceAccountKey, err := config.clientIAM.GetServiceAccountKey(ctx, serviceAccountEmail, uniqueId, opts...)
	if err != nil {
		return err
	}

	d.SetId(serviceAccountKey.Name)
	d.Set("name", serviceAccountKey.Name)
	d.Set("unique_id", strings.Split(serviceAccountKey.Name, "/")[5])
	d.Set("public_key", string(serviceAccountKey.PublicKeyData))

	return nil
}

func resourceServiceAccountKeyDelete(d *schema.ResourceData, meta interface{}) error {
	config := meta.(*Config)
	uniqueId := d.Get("unique_id").(string)
	serviceAccountEmail, err := serviceAccountEmail(d.Get("service_account").(string), d, config)
	if err != nil {
		return err
	}

	project, err := serviceAccountEmailProject(serviceAccountEmail)
	if err != nil {
		return err
	}

	log.Printf("[DEBUG]: Deleting service account key %s of %s", uniqueId, serviceAccountEmail)

	opts := []iam.DeleteServiceAccountKeyOption{
		iam.WithDeleteServiceAccountKeyProject(project),
	}

	ctx := context.Background()
	err = config.clientIAM.DeleteServiceAccountKey(ctx, serviceAccountEmail, uniqueId, opts...)
	if err != nil {
		return err
	}

	d.SetId("")

	return nil
}
