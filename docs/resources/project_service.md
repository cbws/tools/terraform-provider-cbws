---
# generated by https://github.com/hashicorp/terraform-plugin-docs
page_title: "cbws_project_service Resource - terraform-provider-cbws"
subcategory: ""
description: |-

---

# cbws_project_service (Resource)





<!-- schema generated by tfplugindocs -->
## Schema

### Required

- **project** (String) The name. Changing this forces a new project to be created.
- **service** (String) The name. Changing this forces a new project to be created.

### Optional

- **id** (String) The ID of this resource.
- **timeouts** (Block, Optional) (see [below for nested schema](#nestedblock--timeouts))

### Read-Only

- **name** (String) The resource name of the project.

<a id="nestedblock--timeouts"></a>
### Nested Schema for `timeouts`

Optional:

- **create** (String)
- **delete** (String)
- **read** (String)
