---
# generated by https://github.com/hashicorp/terraform-plugin-docs
page_title: "cbws_virtual_machines_instance Resource - terraform-provider-cbws"
subcategory: ""
description: |-

---

# cbws_virtual_machines_instance (Resource)





<!-- schema generated by tfplugindocs -->
## Schema

### Required

- **cpu** (Number) The amount of CPU of the instance.
- **hostname** (String) The hostname of the instance.
- **memory** (Number) The amount of memory of the instance.
- **name** (String) The name. Changing this forces a new instance to be created.
- **root_disk** (Number) The amount of root disk of the instance.

### Optional

- **affinity** (Block Set, Max: 1) The address the instance. (see [below for nested schema](#nestedblock--affinity))
- **id** (String) The ID of this resource.
- **labels** (Map of String) A list of labels useful for filtering, affinities and load balancing
- **ssh_keys** (List of String) The address the instance.
- **timeouts** (Block, Optional) (see [below for nested schema](#nestedblock--timeouts))

### Read-Only

- **address** (String) The address the instance.

<a id="nestedblock--affinity"></a>
### Nested Schema for `affinity`

Optional:

- **node_affinity** (Block Set, Max: 1) (see [below for nested schema](#nestedblock--affinity--node_affinity))

<a id="nestedblock--affinity--node_affinity"></a>
### Nested Schema for `affinity.node_affinity`

Optional:

- **preferred_during_scheduling_ignored_during_execution** (Block Set, Max: 1) The scheduler will prefer to schedule instances to nodes that satisfy the affinity expressions specified by this field, but it may choose a node that violates one or more of the expressions. The node that is most preferred is the one with the greatest sum of weights, i.e. for each node that meets all of the scheduling requirements (resource request, requiredDuringScheduling affinity expressions, etc.), compute a sum by iterating through the elements of this field and adding "weight" to the sum if the node matches the corresponding matchExpressions; the node(s) with the highest sum are the most preferred (see [below for nested schema](#nestedblock--affinity--node_affinity--preferred_during_scheduling_ignored_during_execution))
- **required_during_scheduling_ignored_during_execution** (Block Set, Max: 1) If the affinity requirements specified by this field are not met at scheduling time, the instance will not be scheduled onto the node. If the affinity requirements specified by this field cease to be met at some point during instance execution (e.g. due to an update), the system may or may not try to eventually evict the instance from its node (see [below for nested schema](#nestedblock--affinity--node_affinity--required_during_scheduling_ignored_during_execution))

<a id="nestedblock--affinity--node_affinity--preferred_during_scheduling_ignored_during_execution"></a>
### Nested Schema for `affinity.node_affinity.preferred_during_scheduling_ignored_during_execution`

Optional:

- **preferred_scheduling_term** (Block List) A list of node selector terms. The terms are ORed. (see [below for nested schema](#nestedblock--affinity--node_affinity--preferred_during_scheduling_ignored_during_execution--preferred_scheduling_term))

<a id="nestedblock--affinity--node_affinity--preferred_during_scheduling_ignored_during_execution--preferred_scheduling_term"></a>
### Nested Schema for `affinity.node_affinity.preferred_during_scheduling_ignored_during_execution.preferred_scheduling_term`

Required:

- **preference** (Block Set, Min: 1) A node selector term, associated with the corresponding weight (see [below for nested schema](#nestedblock--affinity--node_affinity--preferred_during_scheduling_ignored_during_execution--preferred_scheduling_term--preference))

Optional:

- **weight** (Number) Weight associated with matching the corresponding nodeSelectorTerm, in the range 1-100

<a id="nestedblock--affinity--node_affinity--preferred_during_scheduling_ignored_during_execution--preferred_scheduling_term--preference"></a>
### Nested Schema for `affinity.node_affinity.preferred_during_scheduling_ignored_during_execution.preferred_scheduling_term.preference`

Optional:

- **match_expression** (Block List) A list of node selector requirements by node's labels (see [below for nested schema](#nestedblock--affinity--node_affinity--preferred_during_scheduling_ignored_during_execution--preferred_scheduling_term--preference--match_expression))

<a id="nestedblock--affinity--node_affinity--preferred_during_scheduling_ignored_during_execution--preferred_scheduling_term--preference--match_expression"></a>
### Nested Schema for `affinity.node_affinity.preferred_during_scheduling_ignored_during_execution.preferred_scheduling_term.preference.match_expression`

Required:

- **key** (String) The label key that the selector applies to
- **operator** (String) Represents a key's relationship to a set of values. Valid operators are In, NotIn, Exists, DoesNotExist. GreaterThan, and LessThan

Optional:

- **values** (List of String) An array of string values. If the operator is In or NotIn, the values array must be non-empty. If the operator is Exists or DoesNotExist, the values array must be empty. If the operator is Gt or Lt, the values array must have a single element, which will be interpreted as an integer.





<a id="nestedblock--affinity--node_affinity--required_during_scheduling_ignored_during_execution"></a>
### Nested Schema for `affinity.node_affinity.required_during_scheduling_ignored_during_execution`

Optional:

- **node_selector_term** (Block List) A list of node selector terms. The terms are ORed. (see [below for nested schema](#nestedblock--affinity--node_affinity--required_during_scheduling_ignored_during_execution--node_selector_term))

<a id="nestedblock--affinity--node_affinity--required_during_scheduling_ignored_during_execution--node_selector_term"></a>
### Nested Schema for `affinity.node_affinity.required_during_scheduling_ignored_during_execution.node_selector_term`

Optional:

- **match_expression** (Block List) A list of node selector requirements by node's labels (see [below for nested schema](#nestedblock--affinity--node_affinity--required_during_scheduling_ignored_during_execution--node_selector_term--match_expression))

<a id="nestedblock--affinity--node_affinity--required_during_scheduling_ignored_during_execution--node_selector_term--match_expression"></a>
### Nested Schema for `affinity.node_affinity.required_during_scheduling_ignored_during_execution.node_selector_term.match_expression`

Required:

- **key** (String) The label key that the selector applies to
- **operator** (String) Represents a key's relationship to a set of values. Valid operators are In, NotIn, Exists, DoesNotExist. GreaterThan, and LessThan

Optional:

- **values** (List of String) An array of string values. If the operator is In or NotIn, the values array must be non-empty. If the operator is Exists or DoesNotExist, the values array must be empty. If the operator is Gt or Lt, the values array must have a single element, which will be interpreted as an integer.






<a id="nestedblock--timeouts"></a>
### Nested Schema for `timeouts`

Optional:

- **create** (String)
- **delete** (String)
- **read** (String)
